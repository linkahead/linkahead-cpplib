get_filename_component(LINKAHEAD_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

include(CMakeFindDependencyMacro)

find_dependency(Threads REQUIRED)
find_dependency(Protobuf CONFIG REQUIRED)
find_dependency(gRPC CONFIG REQUIRED)

if(NOT TARGET linkahead::linkahead)
    include("${LINKAHEAD_CMAKE_DIR}/linkaheadTargets.cmake")
endif()
