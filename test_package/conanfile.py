from conan import ConanFile
from conan.tools.cmake import CMake, cmake_layout, CMakeDeps, CMakeToolchain
from conan.tools.files import copy
from conan.tools.build import cross_building


class LiblinkaheadTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)
        self.test_requires("gtest/1.11.0")
        self.requires("grpc/1.48.4")
        self.requires("protobuf/3.21.12")
        self.requires("boost/1.80.0")

    def layout(self):
        cmake_layout(self, src_folder=".")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

        cmake = CMakeDeps(self)
        cmake.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    # def imports(self):
    #     copy("*.dll", dst="bin", src="bin")
    #     copy("*.dylib*", dst="bin", src="lib")
    #     copy('*.so*', dst='bin', src='lib')

    def test(self):
        if not cross_building(self):
            cmake = CMake(self)
            cmake.test()
