{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "LinkAhead Client Configuration",
  "description": "Configuration of the connection, logging, and other things of a LinkAhead client.",
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "connections": {
      "type": "object",
      "description": "Named connection configurations.",
      "properties": {
        "default": {
          "oneOf": [
            {
              "type": "string",
              "description": "Name of the default connection."
            }, {
              "$ref": "#/definitions/connection_configuration"
            }
          ]
        }
      },
      "additionalProperties": {
        "$ref": "#/definitions/connection_configuration"
      }
    },
    "logging": {
      "type": "object",
      "description": "Configuration of logging",
      "properties": {
        "level": {
          "allOf": [
            {
              "$ref": "#/definitions/log_level_configuration"
            },
            {
            "default": "warn",
            "description": "Global severity filter. Only log messages with at least the given severity are processed by the logging framework. 'off' means that the logging frame work is turned off entirely. Defaults to "
            }
          ]
        },
        "sinks": {
          "type": "object",
          "description": "Configuration of sinks, i.e. the log files, console or other logging frame works where the log messages are supposed to end up eventually.",
          "additionalProperties": {
            "$ref": "#/definitions/sink_configuration"
          }
        }
      }
    },
    "extension": {
      "type": "object",
      "description": "A reserved configuration object which may be used to store additional options for particular clients and special extensions.",
      "additionalProperties": true
    }
  },
  "definitions": {
    "log_level_configuration": {
      "type": "string",
      "enum": ["all", "trace", "debug", "info", "warn", "error", "fatal", "off"]
    },
    "sink_configuration": {
      "type": "object",
      "description": "A single sink configuration.",
      "additionalProperties": false,
      "properties": {
        "level": {
          "allOf": [
            {
              "$ref": "#/definitions/log_level_configuration"
            },
            {
            "description": "Sink severity filter. Only log messages with at least the given severity are passed to this sink. 'off' means that this sink is turned off entirely. Defaults to the global log level."
            }
          ]
        },
        "destination": {
          "type": "string",
          "enum": [ "console", "file", "syslog"],
          "description": "The type of sink-backend. 'console' writes to the STDERR, 'file' to a text file and 'syslog' uses the syslog API."
        },
        "directory": {
          "type": "string",
          "description": "Path to local directory. All log files are being stored to this directory. If not specified, the current working directory will be used."
        },
        "local_address": {
          "type": "string",
          "description": "Local IP address to initiate connection to the syslog server. If not specified, the default local address will be used."
        },
        "target_address": {
          "type": "string",
          "description": "Remote IP address of the syslog server. If not specified, the local address will be used. "
        }
      },
      "required": [ "destination" ],
      "allOf": [ {
          "if": {"properties": {"destination": { "const": "console" } } },
          "then": {"propertyNames" : { "enum": ["level", "destination"] } }
        },
        {
          "if": {"properties": {"destination": { "const": "file" } } },
          "then": {"propertyNames" : { "enum": ["level", "destination", "directory"] } }
        },
        {
          "if": {"properties": {"destination": { "const": "syslog" } } },
          "then": {"propertyNames" : { "enum": ["level", "destination", "target_address", "local_address"] } }
        }
      ]
    },
    "connection_configuration": {
      "type": "object",
      "description": "A single connection configuration.",
      "additionalProperties": false,
      "properties": {
        "name": { "type": "string" },
        "host": {
          "type": "string",
          "description": "Domain name or ip address of the server host.",
          "default": "localhost",
          "examples": ["localhost", "linkahead.example.com", "192.168.0.123"]
        },
        "port": {
          "type": "integer",
          "description": "Ip port of the grpc end-point of the LinkAhead server.",
          "mininum": 1,
          "default": 8443,
          "maximum": 65535
        },
        "tls": {
          "type": "boolean",
          "description": "Indicates that the connection is using TLS (transport layer security) for authentication of the server and encryption of the communication. Setting this to 'false' is insecure unless other mechanism are in place to prevent a man-in-the-middle attack and eavesdropping by an unauthorized agent.",
          "default": true
        },
        "server_certificate_path": {
          "type": "string",
          "description": "Relative or absolute path to a public certificate of a trusted CA (certificate authority) in PEM format. If not specified, the client system's default directory for certificates will be scanned (that is '/etc/ssl/certs/' in many linux distros).",
          "default": null
        },
        "authentication": {
          "type": "object",
          "description": "Configuration of the user authentication. If the authentication property is not set, the client attempts to connect to the server as an anonymous user.",
          "additionalProperties": false,
          "properties": {
            "type": {
              "type": "string",
              "enum": ["plain"]
            },
            "username": { "type": "string" },
            "password": { "type": "string" },
            "auth_token": { "type": "string" }
          },
          "allOf": [
            {
              "if": {"properties": {"type": { "pattern": "^plain$" } } },
              "then": {"required": ["username", "password"] }
            }
          ]
        }
      }
    }
  }
}
