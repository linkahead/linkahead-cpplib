/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 */

#ifndef CLINKAHEAD_H
#define CLINKAHEAD_H
#ifdef __cplusplus
#include <cstdint> // for int64_t
extern "C" {
#else
#include <stdint.h>  // for int64_t
#include <stdbool.h> // for bool
#endif

/**
 * Return the constant linkahead::LIBLINKAHEAD_VERSION_MAJOR.
 */
int linkahead_constants_LIBLINKAHEAD_VERSION_MAJOR();
/**
 * Return the constant linkahead::LIBLINKAHEAD_VERSION_MINOR
 */
int linkahead_constants_LIBLINKAHEAD_VERSION_MINOR();
/**
 * Return the constant linkahead::LIBLINKAHEAD_VERSION_PATCH.
 */
int linkahead_constants_LIBLINKAHEAD_VERSION_PATCH();
/**
 * Return the constant linkahead::COMPATIBLE_SERVER_VERSION_MAJOR.
 */
int linkahead_constants_COMPATIBLE_SERVER_VERSION_MAJOR();
/**
 * Return the constant linkahead::COMPATIBLE_SERVER_VERSION_MINOR.
 */
int linkahead_constants_COMPATIBLE_SERVER_VERSION_MINOR();
/**
 * Return the constant linkahead::COMPATIBLE_SERVER_VERSION_PATCH.
 */
int linkahead_constants_COMPATIBLE_SERVER_VERSION_PATCH();
/**
 * Return the constant linkahead::COMPATIBLE_SERVER_VERSION_PRE_RELEASE.
 */
const char *linkahead_constants_COMPATIBLE_SERVER_VERSION_PRE_RELEASE();

/**
 * Return the status code reserved for errors in clients wrapping this
 * interface.
 */
int linkahead_status_code_OTHER_CLIENT_ERROR();

/**
 * A wrapper of the C++ Connection class.
 *
 * We use a wrapper for future extensibility and in order to have a minimal
 * capability for type checking in C even though the C++ class
 * Connection is opaque in C.
 */
typedef struct linkahead_connection_connection {
  void *wrapped_connection;
  bool _deletable;
} linkahead_connection_connection;

/**
 * A wrapper of the C++ ConnectionConfiguration class.
 *
 * We use a wrapper for future extensibility and in order to have a minimal
 * capability for type checking in C even though the C++ class
 * Connection is opaque in C.
 */
typedef struct linkahead_connection_connection_configuration {
  void *wrapped_connection_configuration;
  bool _deletable;
} linkahead_connection_connection_configuration;

/**
 * A wrapper of the C++ VersionInfo class.
 *
 * We use a wrapper for future extensibility and in order to have a minimal
 * capability for type checking in C even though the C++ class
 * Connection is opaque in C.
 */
typedef struct linkahead_info_version_info {
  int major;
  int minor;
  int patch;
  const char *pre_release;
  const char *build;
} linkahead_info_version_info;

typedef struct linkahead_connection_certificate_provider {
  void *wrapped_certificate_provider;
  bool _deletable;
} linkahead_connection_certificate_provider;

typedef struct linkahead_authentication_authenticator {
  void *wrapped_authenticator;
  bool _deletable;
} linkahead_authentication_authenticator;

/**
 * Return the environment variable of the given name.
 *
 * If the environment variable is not set, return the fallback instead.
 */
const char *linkahead_utility_get_env_fallback(const char *name, const char *fallback);

/**
 * Return a description of the status code.
 */
const char *linkahead_get_status_description(int code);

/**
 * Create a pem-file certificate provider.
 *
 * Use the destructor function
 * `linkahead_connection_delete_certificate_provider` to free the wrapped
 * provider.
 *
 * EXPERT USE ONLY.  Memory management with this function is a bit tricky.
 * Only use it when you know what you are doing.
 */
int linkahead_connection_create_pem_file_certificate_provider(
  linkahead_connection_certificate_provider *out, const char *path);

/**
 * Destructor function for a certificate provider.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_connection_delete_certificate_provider(
  linkahead_connection_certificate_provider *provider);

/**
 * Create a tls-secured connection configuration.
 *
 * The configuration is needed to instantiate a connection.
 *
 * Use the destructor function
 * `linkahead_connection_delete_connection_configuration` to free the wrapped
 * configuration.
 *
 * EXPERT USE ONLY.  Memory management with this function is a bit tricky.
 * Only use it when you know what you are doing.
 */
int linkahead_connection_create_tls_connection_configuration(
  linkahead_connection_connection_configuration *out, const char *host, const int port,
  linkahead_authentication_authenticator *authenticator,
  linkahead_connection_certificate_provider *provider);

/**
 * Create a tls-secured connection configuration.
 *
 * The configuration is needed to instantiate a connection.
 *
 * Use `linkahead_connection_create_tls_connection_configuration` for a
 * tls-secured connection which also supports authentication.
 *
 * Use the destructor function
 * `linkahead_connection_delete_connection_configuration` to free the wrapped
 * configuration.
 *
 * EXPERT USE ONLY.  Memory management with this function is a bit tricky.
 * Only use it when you know what you are doing.
 */
int linkahead_connection_create_insecure_connection_configuration(
  linkahead_connection_connection_configuration *out, const char *host, const int port);

/**
 * Destructor function for the linkahead_connection_connection_configuration
 * struct.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_connection_delete_connection_configuration(
  linkahead_connection_connection_configuration *configuration);

/**
 * Add a public certificate of a trusted certificate authority to an
 * existing, tls-enabled connection configuration.
 *
 * @param cacert path to a pem-file.
 */
int linkahead_connection_configuration_add_cacert(
  linkahead_connection_connection_configuration *configuration, const char *cacert);

/**
 * Create a plain password authenticator.
 *
 * Use the destructor function
 * `linkahead_authentication_delete_authenticator` to free the wrapped
 * authenticator.
 *
 * EXPERT USE ONLY.  Memory management with this function is a bit tricky.
 * Only use it when you know what you are doing.
 */
int linkahead_authentication_create_plain_password_authenticator(
  linkahead_authentication_authenticator *out, const char *username, const char *password);

/**
 * Destructor function for the linkahead_authentication_authenticator struct.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_authentication_delete_authenticator(
  linkahead_authentication_authenticator *authenticator);

/**
 * Create a connection instance.
 *
 * The connection is needed to create transactions and to initiate any other
 * interaction with a LinkAhead server.
 *
 * Use the destructor function
 * `linkahead_connection_delete_connection` to free the wrapped
 * connection.
 *
 * EXPERT USE ONLY.  Memory management with this function is a bit tricky.
 * Only use it when you know what you are doing.
 */
int linkahead_connection_create_connection(
  linkahead_connection_connection *out,
  const linkahead_connection_connection_configuration *configuration);

/**
 * Destructor function for the linkahead_connection_connection struct.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_connection_delete_connection(linkahead_connection_connection *connection);

/**
 * Request the version of the server.
 */
int linkahead_connection_get_version_info(linkahead_info_version_info *out,
                                          const linkahead_connection_connection *connection);

/**
 * Get the default connection from the ConnectionManager.
 *
 * The default connection is to be specified in a configuration file.
 */
int linkahead_connection_connection_manager_get_default_connection(
  linkahead_connection_connection *out);

/**
 * Get a named connection from the ConnectionManager.
 *
 * The named connection is to be specified in a configuration file.
 */
int linkahead_connection_connection_manager_get_connection(linkahead_connection_connection *out,
                                                           const char *name);

/****************************************************************************
 * ENTITY STUFF AND TRANSACTIONS
 ****************************************************************************/

typedef struct linkahead_transaction_transaction {
  void *wrapped_transaction;
  bool _deletable;
} linkahead_transaction_transaction;

/**
 * Create a transaction on an existing connection.
 *
 * This transaction has to be deleted manually by
 * linkahead_transaction_delete_transaction() later on.
 */
int linkahead_connection_connection_create_transaction(linkahead_connection_connection *connection,
                                                       linkahead_transaction_transaction *out);
int linkahead_transaction_delete_transaction(linkahead_transaction_transaction *transaction);
int linkahead_transaction_transaction_retrieve_by_id(linkahead_transaction_transaction *transaction,
                                                     const char *id);
int linkahead_transaction_transaction_retrieve_and_download_file_by_id(
  linkahead_transaction_transaction *transaction, const char *id, const char *path);
int linkahead_transaction_transaction_retrieve_by_ids(
  linkahead_transaction_transaction *transaction, const char *ids[], int length);
int linkahead_transaction_transaction_query(linkahead_transaction_transaction *transaction,
                                            const char *query);
int linkahead_transaction_transaction_execute(linkahead_transaction_transaction *transaction);
// TODO(fspreck) execute_asynchronously may be added as a separate
// function once we actually support asynchronous execution.

typedef struct linkahead_transaction_result_set {
  void *wrapped_result_set;
  bool _deletable;
} linkahead_transaction_result_set;

typedef struct linkahead_entity_entity {
  void *wrapped_entity;
  bool _deletable;
} linkahead_entity_entity;

int linkahead_transaction_transaction_get_result_set(linkahead_transaction_transaction *transaction,
                                                     linkahead_transaction_result_set *out);
/**
 * Release the result set from the transaction.
 *
 * The transactions is spoiled after this action and should not be used anymore.
 *
 * Note: The result_set has to be deleted via linkahead_transaction_delete_result_set.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_transaction_transaction_release_result_set(
  linkahead_transaction_transaction *transaction, linkahead_transaction_result_set *out);
/**
 * Release the entity from the result set.
 *
 * Each entity (each index) can be released once. The result set is spoiled
 * after this action and should not be used for anything else anymore.
 *
 * Note: The result_set has to be deleted via dentity_delete_entity.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_transaction_result_set_release_at(linkahead_transaction_result_set *result_set,
                                                linkahead_entity_entity *entity, int index);
/**
 * Destructor for linkahead_transaction_result_set.
 *
 * EXPERT USE ONLY.  Only use it when you know what you are doing.
 */
int linkahead_transaction_delete_result_set(linkahead_transaction_result_set *result_set);

int linkahead_transaction_transaction_get_count_result(
  linkahead_transaction_transaction *transaction, long *out);

int linkahead_transaction_result_set_at(linkahead_transaction_result_set *result_set,
                                        linkahead_entity_entity *entity, int index);
int linkahead_transaction_result_set_size(linkahead_transaction_result_set *result_set, int *out);

int linkahead_transaction_transaction_insert_entity(linkahead_transaction_transaction *transaction,
                                                    linkahead_entity_entity *entity);
int linkahead_transaction_transaction_update_entity(linkahead_transaction_transaction *transaction,
                                                    linkahead_entity_entity *entity);
int linkahead_transaction_transaction_delete_by_id(linkahead_transaction_transaction *transaction,
                                                   const char *id);

typedef struct linkahead_entity_property {
  void *wrapped_property;
  bool _deletable;
} linkahead_entity_property;

typedef struct linkahead_entity_parent {
  void *wrapped_parent;
  bool _deletable;
} linkahead_entity_parent;

typedef struct linkahead_entity_message {
  void *wrapped_message;
  bool _deletable;
} linkahead_entity_message;

typedef struct linkahead_entity_value {
  void *wrapped_value;
  bool _deletable;
} linkahead_entity_value;

typedef struct linkahead_entity_datatype {
  void *wrapped_datatype;
  bool _deletable;
} linkahead_entity_datatype;

// GETTERS FOR EVERYTHING
int linkahead_entity_entity_get_id(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_role(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_name(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_description(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_local_path(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_datatype(linkahead_entity_entity *entity,
                                         linkahead_entity_datatype *out);
int linkahead_entity_entity_get_unit(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_value(linkahead_entity_entity *entity, linkahead_entity_value *out);
int linkahead_entity_entity_get_version_id(linkahead_entity_entity *entity, char **out);
int linkahead_entity_entity_get_errors_size(linkahead_entity_entity *entity, int *out);
int linkahead_entity_entity_get_error(linkahead_entity_entity *entity,
                                      linkahead_entity_message *out, int index);
int linkahead_entity_entity_get_warnings_size(linkahead_entity_entity *entity, int *out);
int linkahead_entity_entity_get_warning(linkahead_entity_entity *entity,
                                        linkahead_entity_message *out, int index);
int linkahead_entity_entity_get_infos_size(linkahead_entity_entity *entity, int *out);
int linkahead_entity_entity_get_info(linkahead_entity_entity *entity, linkahead_entity_message *out,
                                     int index);
int linkahead_entity_entity_get_properties_size(linkahead_entity_entity *entity, int *out);
int linkahead_entity_entity_get_property(linkahead_entity_entity *entity,
                                         linkahead_entity_property *out, int index);
int linkahead_entity_entity_get_parents_size(linkahead_entity_entity *entity, int *out);
int linkahead_entity_entity_get_parent(linkahead_entity_entity *entity,
                                       linkahead_entity_parent *out, int index);

int linkahead_entity_property_get_id(linkahead_entity_property *property, char **out);
int linkahead_entity_property_get_name(linkahead_entity_property *property, char **out);
int linkahead_entity_property_get_description(linkahead_entity_property *property, char **out);
int linkahead_entity_property_get_importance(linkahead_entity_property *property, char **out);
int linkahead_entity_property_get_datatype(linkahead_entity_property *property,
                                           linkahead_entity_datatype *out);
int linkahead_entity_property_get_unit(linkahead_entity_property *property, char **out);
int linkahead_entity_property_get_value(linkahead_entity_property *property,
                                        linkahead_entity_value *out);
int linkahead_entity_parent_get_id(linkahead_entity_parent *parent, char **out);
int linkahead_entity_parent_get_name(linkahead_entity_parent *parent, char **out);
int linkahead_entity_parent_get_description(linkahead_entity_parent *parent, char **out);

int linkahead_entity_message_get_code(linkahead_entity_message *message, int *out);
int linkahead_entity_message_get_description(linkahead_entity_message *message, char **out);

int linkahead_entity_datatype_is_undefined(linkahead_entity_datatype *datatype, bool *out);
int linkahead_entity_datatype_is_atomic(linkahead_entity_datatype *datatype, bool *out);
int linkahead_entity_datatype_is_reference(linkahead_entity_datatype *datatype, bool *out);
int linkahead_entity_datatype_is_list_of_atomic(linkahead_entity_datatype *datatype, bool *out);
int linkahead_entity_datatype_is_list_of_reference(linkahead_entity_datatype *datatype, bool *out);
int linkahead_entity_datatype_get_datatype_name(linkahead_entity_datatype *datatype, char **out);

int linkahead_entity_value_is_null(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_is_string(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_is_double(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_is_integer(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_is_bool(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_is_vector(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_get_as_string(linkahead_entity_value *value, char **out);
int linkahead_entity_value_get_as_double(linkahead_entity_value *value, double *out);
int linkahead_entity_value_get_as_integer(linkahead_entity_value *value, int64_t *out);
int linkahead_entity_value_get_as_bool(linkahead_entity_value *value, bool *out);
int linkahead_entity_value_get_as_vector_size(linkahead_entity_value *value, int *out);
int linkahead_entity_value_get_as_vector_at(linkahead_entity_value *value,
                                            linkahead_entity_value *out, const int index);

// CONSTRUCTORS AND DESTRUCTORS
int linkahead_entity_create_entity(linkahead_entity_entity *out);
int linkahead_entity_delete_entity(linkahead_entity_entity *out);
int linkahead_entity_create_property(linkahead_entity_property *out);
int linkahead_entity_delete_property(linkahead_entity_property *out);
int linkahead_entity_create_parent(linkahead_entity_parent *out);
int linkahead_entity_delete_parent(linkahead_entity_parent *out);

// DATATYPE CONSTRUCTORS for atomic and reference datatypes and lists thereof
int linkahead_entity_create_atomic_datatype(linkahead_entity_datatype *out, const char *name);
int linkahead_entity_create_reference_datatype(linkahead_entity_datatype *out, const char *name);
int linkahead_entity_create_atomic_list_datatype(linkahead_entity_datatype *out, const char *name);
int linkahead_entity_create_reference_list_datatype(linkahead_entity_datatype *out,
                                                    const char *name);
int linkahead_entity_delete_datatype(linkahead_entity_datatype *out);

// VALUE CONSTRUCTORS (resolve overloaded constructors)
int linkahead_entity_create_int_value(linkahead_entity_value *out, const int64_t value);
int linkahead_entity_create_string_value(linkahead_entity_value *out, const char *value);
int linkahead_entity_create_double_value(linkahead_entity_value *out, const double value);
int linkahead_entity_create_bool_value(linkahead_entity_value *out, const bool value);
int linkahead_entity_create_int_vector_value(linkahead_entity_value *out, const int64_t *value,
                                             const int length);
int linkahead_entity_create_string_vector_value(linkahead_entity_value *out, const char **value,
                                                const int length);
int linkahead_entity_create_double_vector_value(linkahead_entity_value *out, const double *value,
                                                const int length);
int linkahead_entity_create_bool_vector_value(linkahead_entity_value *out, const bool *value,
                                              const int length);
int linkahead_entity_delete_value(linkahead_entity_value *out);

// SETTERS FOR EVERYTHING THAT MAY BE SET
int linkahead_entity_entity_set_role(linkahead_entity_entity *entity, const char *role);
int linkahead_entity_entity_set_name(linkahead_entity_entity *entity, const char *name);
int linkahead_entity_entity_set_description(linkahead_entity_entity *entity,
                                            const char *description);
int linkahead_entity_entity_set_local_path(linkahead_entity_entity *entity, const char *name);
int linkahead_entity_entity_set_file_path(linkahead_entity_entity *entity, const char *name);
int linkahead_entity_entity_set_datatype(linkahead_entity_entity *entity,
                                         linkahead_entity_datatype *datatype);
int linkahead_entity_entity_set_unit(linkahead_entity_entity *entity, const char *unit);
int linkahead_entity_entity_set_value(linkahead_entity_entity *entity,
                                      linkahead_entity_value *value);

int linkahead_entity_entity_append_parent(linkahead_entity_entity *entity,
                                          linkahead_entity_parent *parent);
int linkahead_entity_entity_remove_parent(linkahead_entity_entity *entity, int index);
int linkahead_entity_entity_append_property(linkahead_entity_entity *entity,
                                            linkahead_entity_property *property);
int linkahead_entity_entity_remove_property(linkahead_entity_entity *entity, int index);

int linkahead_entity_property_set_id(linkahead_entity_property *property, const char *id);
int linkahead_entity_property_set_name(linkahead_entity_property *property, const char *name);
int linkahead_entity_property_set_datatype(linkahead_entity_property *property,
                                           linkahead_entity_datatype *datatype);
int linkahead_entity_property_set_importance(linkahead_entity_property *property,
                                             const char *importance);
int linkahead_entity_property_set_unit(linkahead_entity_property *property, const char *unit);
int linkahead_entity_property_set_value(linkahead_entity_property *property,
                                        linkahead_entity_value *value);

int linkahead_entity_parent_set_id(linkahead_entity_parent *parent, const char *id);
int linkahead_entity_parent_set_name(linkahead_entity_parent *parent, const char *name);

#ifdef __cplusplus
}
#endif
#endif
