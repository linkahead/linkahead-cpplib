/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @brief File descriptors are used to represent directories, links and files
 * of the linkahead file system and their meta data.
 * @file linkahead/entity.h
 * @author Timm Fitchen
 * @date 2022-01-21
 */
#ifndef LINKAHEAD_FILE_DESCRIPTOR_H
#define LINKAHEAD_FILE_DESCRIPTOR_H

#include "caosdb/entity/v1/main.pb.h" // for RepeatedPtrField
#include <filesystem>                 // for path

namespace linkahead::entity {
using ProtoFileDescriptor = caosdb::entity::v1::FileDescriptor;
using caosdb::entity::v1::FileTransmissionId;

struct FileDescriptor {
  FileTransmissionId *file_transmission_id;
  ProtoFileDescriptor *wrapped;
  std::filesystem::path local_path;
};

} // namespace linkahead::entity
#endif
