/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_INFO_H
#define LINKAHEAD_INFO_H
/**
 * @file linkahead/info.h
 * @author Timm Fitschen
 * @date 2021-07-02
 * @brief General information about the LinkAheadServer.
 */
#include "caosdb/info/v1/main.pb.h" // for VersionInfo
#include <cstdint>                  // for uint32_t
#include <string>                   // for string

namespace linkahead::info {

using ProtoVersionInfo = caosdb::info::v1::VersionInfo;

/**
 * A read-only version object which represents the version of the server.
 *
 * The version info follows semantic versioning (SemVer 2.0). Wrapper class for
 * the VersionInfo protobuf.
 *
 * @brief A read-only version object which represents the version of the server.
 */
class VersionInfo {
public:
  /**
   * Wrapp a Protobuf VersionInfo object.
   *
   * Don't instantiate this version info class. The constructor is only public
   * for simpler testing. Create a LinkAheadConnection and use
   * LinkAheadConnection::GetVersionInfo() instead to get the version of the
   * server behind the given connection.
   */
  explicit inline VersionInfo(ProtoVersionInfo *info) : info(info){};
  [[nodiscard]] inline auto GetMajor() const -> int32_t { return this->info->major(); }
  [[nodiscard]] inline auto GetMinor() const -> int32_t { return this->info->minor(); }
  [[nodiscard]] inline auto GetPatch() const -> int32_t { return this->info->patch(); }
  [[nodiscard]] inline auto GetPreRelease() const -> const std::string & {
    return this->info->pre_release();
  }
  [[nodiscard]] inline auto GetBuild() const -> const std::string & { return this->info->build(); }

private:
  /// This object is the owner of the Protobuf VersionInfo message.
  std::unique_ptr<ProtoVersionInfo> info;
};

} // namespace linkahead::info
#endif
