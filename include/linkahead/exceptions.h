/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_EXCEPTIONS_H
#define LINKAHEAD_EXCEPTIONS_H
#include "linkahead/status_code.h"
#include <stdexcept>
#include <string>

namespace linkahead::exceptions {
using linkahead::StatusCode;
using std::runtime_error;

/**
 * @brief Generic exception class of the linkahead client library.
 */
class Exception : public runtime_error {
public:
  explicit Exception(StatusCode code, const std::string &what_arg)
    : runtime_error(what_arg), code(code) {}
  [[nodiscard]] inline auto GetCode() const -> StatusCode { return this->code; }

private:
  StatusCode code;
};

/**
 * @brief Exception for authentication errors.
 */
class AuthenticationError : public Exception {
public:
  explicit AuthenticationError(const std::string &what_arg)
    : Exception(StatusCode::AUTHENTICATION_ERROR, what_arg) {}
};

/**
 * @brief The connection to the LinkAhead server is down.
 */
class ConnectionError : public Exception {
public:
  explicit ConnectionError(const std::string &what_arg)
    : Exception(StatusCode::CONNECTION_ERROR, what_arg) {}
};

/**
 * @brief The transaction terminated unsuccessfully.
 */
class TransactionError : public Exception {
protected:
  TransactionError(StatusCode code, const std::string &what_arg) : Exception(code, what_arg) {}

public:
  explicit TransactionError(const std::string &what_arg)
    : Exception(StatusCode::GENERIC_TRANSACTION_ERROR, what_arg) {}
};

/**
 * @brief The transaction is in the wrong state for your action.
 */
class TransactionStatusError : public Exception {
public:
  explicit TransactionStatusError(const std::string &what_arg)
    : Exception(StatusCode::TRANSACTION_STATUS_ERROR, what_arg) {}
};

/**
 * @brief The transaction has a wrong type for your action.
 */
class TransactionTypeError : public Exception {
public:
  explicit TransactionTypeError(const std::string &what_arg)
    : Exception(StatusCode::TRANSACTION_TYPE_ERROR, what_arg) {}
};

/**
 * @brief Exception for errors of the ConfigurationManager or other components
 * of the configuration.
 */
class ConfigurationError : public Exception {
public:
  explicit ConfigurationError(const std::string &what_arg)
    : Exception(StatusCode::CONFIGURATION_ERROR, what_arg) {}
};

/**
 * @brief Exception for errors during the configuration of the connection.
 */
class ConnectionConfigurationError : public Exception {
public:
  explicit ConnectionConfigurationError(const std::string &what_arg)
    : Exception(StatusCode::CONNECTION_CONFIGURATION_ERROR, what_arg) {}
};

} // namespace linkahead::exceptions
#endif
