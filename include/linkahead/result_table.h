/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef LINKAHEAD_RESULT_TABLE_H
#define LINKAHEAD_RESULT_TABLE_H

#include <iosfwd>            // for ptrdiff_t
#include <iterator>          // for output_iterator_tag
#include <memory>            // for unique_ptr
#include <string>            // for string
#include "linkahead/value.h" // for Value

namespace linkahead::transaction {
using linkahead::entity::Value;

class ResultTableImpl;

class ResultTableColumnImpl;

class ResultTableRowImpl;

class ResultTableRow {
public:
  [[nodiscard]] auto GetValue(const std::string &column) const noexcept -> Value;
  friend class ResultTable;
  explicit ResultTableRow(std::unique_ptr<ResultTableRowImpl> delegate);

private:
  std::unique_ptr<ResultTableRowImpl> delegate;
};

class ResultTableColumn {
public:
  /**
   * Get the name of the column.
   */
  [[nodiscard]] auto GetName() const noexcept -> const std::string &;

  friend class ResultTable;
  explicit ResultTableColumn(std::unique_ptr<ResultTableColumnImpl> delegate);

private:
  std::unique_ptr<ResultTableColumnImpl> delegate;
};

class ResultTable {
  class HeaderIterator;
  class RowIterator;

public:
  /**
   * Number of rows.
   *
   * The header is not counted as a row.
   */
  [[nodiscard]] auto size() const noexcept -> int;
  /**
   * Get the header of this table, i.e. the list of columns.
   */
  [[nodiscard]] auto GetHeader() const noexcept -> HeaderIterator;
  /**
   * Get the data rows, i.e. the actual data.
   */
  [[nodiscard]] auto GetRows() const noexcept -> RowIterator;

  friend class ResultTableImpl;

private:
  class HeaderIterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = ResultTableColumn;
    using difference_type = std::ptrdiff_t;
    using pointer = ResultTableColumn *;
    using reference = ResultTableColumn &;

    explicit HeaderIterator(const ResultTable *result_table, int index = 0);
    HeaderIterator(const HeaderIterator &other);
    auto operator*() const -> const ResultTableColumn &;
    auto operator++() -> HeaderIterator &;
    auto operator++(int) -> HeaderIterator;
    auto operator!=(const HeaderIterator &rhs) const -> bool;
    auto size() const noexcept -> int;
    auto begin() const -> HeaderIterator;
    auto end() const -> HeaderIterator;

  private:
    int current_index = 0;
    const ResultTable *result_table;
  };

  class RowIterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = linkahead::transaction::ResultTableRow;
    using difference_type = std::ptrdiff_t;
    using pointer = value_type *;
    using reference = value_type &;

    explicit RowIterator(const ResultTable *result_table, int index = 0);
    RowIterator(const RowIterator &other);
    auto operator*() const -> const ResultTableRow &;
    auto operator++() -> RowIterator &;
    auto operator++(int) -> RowIterator;
    auto operator!=(const RowIterator &rhs) const -> bool;
    auto size() const noexcept -> int;
    auto begin() const -> RowIterator;
    auto end() const -> RowIterator;

  private:
    int current_index = 0;
    const ResultTable *result_table;
  };

  explicit ResultTable(std::unique_ptr<ResultTableImpl> delegate);
  std::unique_ptr<ResultTableImpl> delegate;
};

} // namespace linkahead::transaction
#endif
