/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_VALUE_H
#define LINKAHEAD_VALUE_H
#include "linkahead/protobuf_helper.h" // for ProtoMessageWrapper
#include "caosdb/entity/v1/main.pb.h"  // for RepeatedPtrField, Message
#include <cstdint>                     // for int64_t
#include <google/protobuf/arena.h>     // for Arena
#include <memory>                      // for unique_ptr
#include <string>                      // for string, operator==
#include <utility>                     // for move
#include <vector>                      // for vector

#define LIST_VALUE_CONSTRUCTOR(TYPE, SETTER)                                                       \
  explicit inline Value(const std::vector<TYPE> &values)                                           \
    : ScalarProtoMessageWrapper<ProtoValue>() {                                                    \
    for (const auto &value : values) {                                                             \
      this->wrapped->mutable_list_values()->add_values()->SETTER(value);                           \
    }                                                                                              \
    if (values.empty()) {                                                                          \
      this->wrapped->mutable_list_values()->add_values()->set_special_value(                       \
        ProtoSpecialValue::SPECIAL_VALUE_UNSPECIFIED);                                             \
    }                                                                                              \
  }

namespace linkahead::entity {
using google::protobuf::Arena;
using linkahead::utility::get_arena;
using linkahead::utility::ProtoMessageWrapper;
using linkahead::utility::ScalarProtoMessageWrapper;
using ProtoSpecialValue = caosdb::entity::v1::SpecialValue;
using ProtoValue = caosdb::entity::v1::Value;
using ProtoScalarValue = caosdb::entity::v1::ScalarValue;
using ValueCase = caosdb::entity::v1::Value::ValueCase;
using ScalarValueCase = caosdb::entity::v1::ScalarValue::ScalarValueCase;

class ScalarValue;
class Value;

/**
 * Pure abstract base class for values.
 */
class AbstractValue {
public:
  /**
   * Pure virtual destructor.
   */
  virtual ~AbstractValue() = 0;
  /**
   * Return true iff the value is a NULL value (NULL in the LinkAhead sense).
   */
  [[nodiscard]] virtual auto IsNull() const noexcept -> bool = 0;
  /**
   * Return true iff the value is best represented in C++ types as a
   * std::string.
   *
   * If true, clients may call GetAsString to receive a std::string
   * representation of the value.
   */
  [[nodiscard]] virtual auto IsString() const noexcept -> bool = 0;
  /**
   * Return true iff the value is best represented in C++ types as a
   * bool.
   *
   * If true, clients may call GetAsBool to receive a bool
   * representation of the value.
   */
  [[nodiscard]] virtual auto IsBool() const noexcept -> bool = 0;
  /**
   * Return true iff the value is best represented in C++ types as a
   * double.
   *
   * If true, clients may call GetAsDouble to receive a double
   * representation of the value.
   */
  [[nodiscard]] virtual auto IsDouble() const noexcept -> bool = 0;
  /**
   * Return true iff the value is best represented in C++ types as an
   * int64_t.
   *
   * If true, clients may call GetAsInt64 to receive an int64_t
   * representation of the value.
   */
  [[nodiscard]] virtual auto IsInt64() const noexcept -> bool = 0;
  /**
   * Return true iff the value is best represented in C++ types as a
   * std::vector<ScalarValue>.
   *
   * If true, clients may call GetAsVector to receive a
   * std::vector<ScalarValue> representation of the value.
   */
  [[nodiscard]] virtual auto IsVector() const noexcept -> bool = 0;

  /**
   * Return a std::string representation of this value.
   *
   * Clients should call IsString before calling this function in order to
   * assure that this value is indeed best represented by a std::string.
   *
   * The return value is undefined if IsString is false.
   */
  [[nodiscard]] virtual auto GetAsString() const noexcept -> const std::string & = 0;
  /**
   * Return a bool representation of this value.
   *
   * Clients should call IsBool before calling this function in order to
   * assure that this value is indeed best represented by a bool.
   *
   * The return value is undefined if IsBool is false.
   */
  [[nodiscard]] virtual auto GetAsBool() const noexcept -> bool = 0;
  /**
   * Return a double representation of this value.
   *
   * Clients should call IsDouble before calling this function in order to
   * assure that this value is indeed best represented by a double.
   *
   * The return value is undefined if IsDouble is false.
   */
  [[nodiscard]] virtual auto GetAsDouble() const noexcept -> double = 0;
  /**
   * Return an int64_t representation of this value.
   *
   * Clients should call IsInt64 before calling this function in order to
   * assure that this value is indeed best represented by an int64_t.
   *
   * The return value is undefined if IsInt64 is false.
   */
  [[nodiscard]] virtual auto GetAsInt64() const noexcept -> int64_t = 0;
  /**
   * Return a std::vector<ScalarValue> representation of this value.
   *
   * Clients should call IsVector before calling this function in order to
   * assure that this value is indeed best represented by a
   * std::vector<ScalarValue>.
   *
   * The return value is undefined if IsVector is false.
   */
  [[nodiscard]] virtual auto GetAsVector() const noexcept -> const std::vector<ScalarValue> & = 0;
  [[nodiscard]] virtual auto ToString() const noexcept -> const std::string = 0;
  friend class Value;

protected:
  [[nodiscard]] virtual auto GetProtoValue() const noexcept -> const ProtoValue * = 0;
};

inline AbstractValue::~AbstractValue() {}

class ScalarValue : public AbstractValue, public ScalarProtoMessageWrapper<ProtoScalarValue> {
public:
  /**
   * Destructor.
   */
  inline ~ScalarValue(){};
  /**
   * Copy constructor.
   */
  inline ScalarValue(const ScalarValue &original) : ScalarValue() {
    this->wrapped->CopyFrom(*original.wrapped);
  }
  /**
   * Move constructor.
   */
  inline ScalarValue(ScalarValue &&other) : ScalarValue(std::move(other.wrapped)){};
  /**
   * Copy assignment operator.
   */
  inline auto operator=(const ScalarValue &original) -> ScalarValue & {
    if (&original != this) {
      this->wrapped->CopyFrom(*original.wrapped);
    }
    return *this;
  }
  /**
   * Move assignment operator.
   */
  inline auto operator=(ScalarValue &&other) -> ScalarValue & {
    if (&other != this) {
      this->wrapped = std::move(other.wrapped);
    }
    return *this;
  }

  inline ScalarValue(ProtoScalarValue *wrapped)
    : ScalarProtoMessageWrapper<ProtoScalarValue>(wrapped), proto_value(nullptr) {}

  [[nodiscard]] inline auto IsNull() const noexcept -> bool {
    return this->wrapped == nullptr ||
           (this->wrapped->scalar_value_case() == ScalarValueCase::kSpecialValue &&
            this->wrapped->special_value() == ProtoSpecialValue::SPECIAL_VALUE_UNSPECIFIED);
  }
  [[nodiscard]] inline auto IsString() const noexcept -> bool {
    return !IsNull() &&
           ((this->wrapped->scalar_value_case() == ScalarValueCase::kStringValue) ||
            (this->wrapped->scalar_value_case() == ScalarValueCase::kSpecialValue &&
             this->wrapped->special_value() == ProtoSpecialValue::SPECIAL_VALUE_EMPTY_STRING));
  }
  [[nodiscard]] inline auto GetAsString() const noexcept -> const std::string & {
    return this->wrapped->string_value();
  }

  [[nodiscard]] inline auto IsDouble() const noexcept -> bool {
    return !IsNull() && (this->wrapped->scalar_value_case() == ScalarValueCase::kDoubleValue);
  }
  [[nodiscard]] inline auto GetAsDouble() const noexcept -> double {
    return this->wrapped->double_value();
  }

  [[nodiscard]] inline auto IsInt64() const noexcept -> bool {
    return !IsNull() && (this->wrapped->scalar_value_case() == ScalarValueCase::kIntegerValue);
  }
  [[nodiscard]] inline auto GetAsInt64() const noexcept -> int64_t {
    return this->wrapped->integer_value();
  }

  [[nodiscard]] inline auto IsBool() const noexcept -> bool {
    return !IsNull() && (this->wrapped->scalar_value_case() == ScalarValueCase::kBooleanValue);
  }
  [[nodiscard]] inline auto GetAsBool() const noexcept -> bool {
    return this->wrapped->boolean_value();
  }
  [[nodiscard]] auto IsVector() const noexcept -> bool {
    // Always false b/c a scalar value is never a collection.
    return false;
  }
  [[nodiscard]] auto GetAsVector() const noexcept -> const std::vector<ScalarValue> & {
    // Always return an empty vector.
    static const std::vector<ScalarValue> empty_collection;
    return empty_collection;
  }
  [[nodiscard]] inline auto ToString() const noexcept -> const std::string {
    return ScalarProtoMessageWrapper::ToString();
  }

  friend class Value;

protected:
  [[nodiscard]] auto GetProtoValue() const noexcept -> const ProtoValue * {
    if (this->proto_value == nullptr) {
      this->proto_value = Arena::CreateMessage<ProtoValue>(get_arena());
      this->proto_value->mutable_scalar_value()->CopyFrom(*this->wrapped);
    }
    return this->proto_value;
  };
  inline ScalarValue() : ScalarProtoMessageWrapper<ProtoScalarValue>(), proto_value(nullptr) {}

private:
  mutable ProtoValue *proto_value;
};

class Value : public AbstractValue, public ScalarProtoMessageWrapper<ProtoValue> {
public:
  /**
   * Copy constructor.
   */
  inline Value(const Value &original) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->CopyFrom(*original.wrapped);
  }
  /**
   * Move constructor.
   */
  inline Value(Value &&other) : Value(std::move(other.wrapped)) {}
  /**
   * Destructor.
   */
  inline ~Value() {}
  inline Value() : ScalarProtoMessageWrapper<ProtoValue>(static_cast<ProtoValue *>(nullptr)) {
    // has NULL_VALUE now
  }
  explicit inline Value(const ScalarValue &value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->CopyFrom(*value.wrapped);
  }
  explicit inline Value(const AbstractValue &value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->CopyFrom(*value.GetProtoValue());
  }
  explicit inline Value(ProtoValue *wrapped) : ScalarProtoMessageWrapper<ProtoValue>(wrapped) {}
  explicit inline Value(const ProtoValue &value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->CopyFrom(value);
  }
  explicit inline Value(const std::string &value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->set_string_value(value);
  }
  explicit inline Value(const char *value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->set_string_value(std::string(value));
  }
  explicit inline Value(double value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->set_double_value(value);
  }
  explicit inline Value(int64_t value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->set_integer_value(value);
  }
  explicit inline Value(int value) : Value(static_cast<int64_t>(value)) {}
  explicit inline Value(bool value) : ScalarProtoMessageWrapper<ProtoValue>() {
    this->wrapped->mutable_scalar_value()->set_boolean_value(value);
  }

  LIST_VALUE_CONSTRUCTOR(int, set_integer_value)
  LIST_VALUE_CONSTRUCTOR(int64_t, set_integer_value)
  LIST_VALUE_CONSTRUCTOR(double, set_double_value)
  LIST_VALUE_CONSTRUCTOR(std::string, set_string_value)
  LIST_VALUE_CONSTRUCTOR(char *, set_string_value)
  LIST_VALUE_CONSTRUCTOR(bool, set_boolean_value)

  [[nodiscard]] inline auto IsNull() const noexcept -> bool {
    return this->wrapped == nullptr ||
           (this->wrapped->value_case() == ValueCase::VALUE_NOT_SET ||
            (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kSpecialValue &&
             this->wrapped->scalar_value().special_value() ==
               ProtoSpecialValue::SPECIAL_VALUE_UNSPECIFIED));
  }

  [[nodiscard]] inline auto IsString() const noexcept -> bool {
    if (!IsNull() && this->wrapped->value_case() == ValueCase::kScalarValue) {

      return (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kStringValue) ||
             (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kSpecialValue &&
              this->wrapped->scalar_value().special_value() ==
                ProtoSpecialValue::SPECIAL_VALUE_EMPTY_STRING);
    }
    return false;
  }
  [[nodiscard]] inline auto GetAsString() const noexcept -> const std::string & {
    if (!IsString()) {
      static std::string empty_string;
      return empty_string;
    }
    return this->wrapped->scalar_value().string_value();
  }

  [[nodiscard]] inline auto IsDouble() const noexcept -> bool {
    if (!IsNull() && this->wrapped->value_case() == ValueCase::kScalarValue) {

      return (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kDoubleValue);
    }
    return false;
  }
  [[nodiscard]] inline auto GetAsDouble() const noexcept -> double {
    if (!IsDouble())
      return 0.0;
    return this->wrapped->scalar_value().double_value();
  }

  [[nodiscard]] inline auto IsInt64() const noexcept -> bool {
    if (!IsNull() && this->wrapped->value_case() == ValueCase::kScalarValue) {

      return (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kIntegerValue);
    }
    return false;
  }
  [[nodiscard]] inline auto GetAsInt64() const noexcept -> int64_t {
    if (!IsInt64())
      return 0;
    return this->wrapped->scalar_value().integer_value();
  }

  [[nodiscard]] inline auto IsBool() const noexcept -> bool {
    if (!IsNull() && this->wrapped->value_case() == ValueCase::kScalarValue) {

      return (this->wrapped->scalar_value().scalar_value_case() == ScalarValueCase::kBooleanValue);
    }
    return false;
  }
  [[nodiscard]] inline auto GetAsBool() const noexcept -> bool {
    if (!IsBool())
      return false;
    return this->wrapped->scalar_value().boolean_value();
  }

  [[nodiscard]] inline auto IsVector() const noexcept -> bool {
    return !IsNull() && this->wrapped->value_case() == ValueCase::kListValues;
  }
  [[nodiscard]] inline auto GetAsVector() const noexcept -> const std::vector<ScalarValue> & {
    if (!IsVector() || (this->wrapped->list_values().values(0).has_special_value() &&
                        this->wrapped->list_values().values(0).special_value() ==
                          ProtoSpecialValue::SPECIAL_VALUE_UNSPECIFIED)) {
      // create empty list
      static std::vector<ScalarValue> empty_values;
      return empty_values;
    }
    if (this->collection_values == nullptr) {
      this->collection_values = std::make_unique<std::vector<ScalarValue>>();
      for (auto &scalar : *(this->wrapped->mutable_list_values()->mutable_values())) {
        this->collection_values->push_back(ScalarValue(&scalar));
      }
    }
    return *(this->collection_values);
  }

  /**
   * Copy assignment operator.
   */
  inline auto operator=(const Value &other) -> Value & {
    if (&other != this) {
      this->wrapped->CopyFrom(*other.wrapped);
      this->collection_values.reset();
    }
    return *this;
  }

  /**
   * Move assignment operator.
   */
  inline auto operator=(Value &&other) -> Value & {
    if (&other != this) {
      this->wrapped = std::move(other.wrapped);
      this->collection_values.reset();
    }
    return *this;
  }

  [[nodiscard]] inline auto ToString() const noexcept -> const std::string {
    return ScalarProtoMessageWrapper::ToString();
  }

  friend class Entity;
  friend class Property;

protected:
  [[nodiscard]] auto GetProtoValue() const noexcept -> const ProtoValue * { return this->wrapped; };

private:
  mutable std::unique_ptr<std::vector<ScalarValue>> collection_values;
};

} // namespace linkahead::entity

#endif
