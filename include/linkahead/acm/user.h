/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @brief Users, together with roles, and permissions are a fundamental concept
 * of the access controll management of LinkAhead.
 *
 * @file linkahead/acm/user.h
 * @author Timm Fitchen
 * @date 2022-06-29
 */
#ifdef BUILD_ACM
#ifndef LINKAHEAD_ACM_USER_H
#define LINKAHEAD_ACM_USER_H

#include <memory> // for unique_ptr
#include <string> // for string

namespace linkahead::connection {
class Connection;
}

namespace linkahead::acm {

/**
 * The UserImpl class is the delegate of the User class. It hides the
 * implementation details from the clients.
 */
class UserImpl;

/**
 * The User class is a delegator. The actual data is stored in a wrapped
 * UserImpl object.
 */
class User {
public:
  /**
   * Default constructor.
   *
   * Initialize a user without any name, realm, password...
   */
  User();
  /**
   * Constructor. Initialize a user in the given realm with the given name.
   */
  explicit User(std::string realm, std::string name);
  /**
   * Constructor. Initialize a user with the given name.
   */
  explicit User(std::string name);
  /**
   * Copy constructor.
   */
  User(const User &user);
  /**
   * Move constructor.
   *
   * The moved-from user is empty, but still usable.
   */
  User(User &&user) noexcept;
  /**
   * Copy assignment.
   */
  auto operator=(const User &user) -> User &;
  /**
   * Move assignment.
   *
   * The moved-from user is empty, but still usable.
   */
  auto operator=(User &&user) noexcept -> User &;
  /**
   * Dtor.
   */
  ~User();

  /**
   * Return a string representation of this user.
   */
  auto ToString() const -> std::string;
  /**
   * Return the name of this user or the empty string.
   */
  [[nodiscard]] auto GetName() const -> const std::string &;
  /**
   * Set the name of this user.
   */
  auto SetName(const std::string &name) -> void;
  /**
   * Return the realm of this user or the empty.
   */
  [[nodiscard]] auto GetRealm() const -> const std::string &;
  /**
   * Set the realm of this user.
   */
  auto SetRealm(const std::string &realm) -> void;
  /**
   * Return the password of this user or the empty string.
   */
  [[nodiscard]] auto GetPassword() const -> const std::string &;
  /**
   * Set the password of this user.
   */
  auto SetPassword(const std::string &password) -> void;

  friend class linkahead::connection::Connection;

private:
  /**
   * Constructor. Create a user object from the given UserImpl delegate.
   */
  explicit User(std::unique_ptr<UserImpl> wrapped);
  /**
   * The UserImpl delegate.
   */
  std::unique_ptr<UserImpl> wrapped;
};

} // namespace linkahead::acm
#endif
#endif
