# Contributing

Thank you very much to all contributers—[past, present](HUMANS.md), and prospective ones.

## Code of Conduct

By participating, you are expected to uphold our [Code of Conduct](CODE_OF_CONDUCT.md).

## How to Contribute

* You found a bug, have a question, or want to request a feature? Please file
  a Gitlab issue.
* You want to contribute code or write documentation? There is no standardized
  way to contribute code yet. Please contact us at <info@indiscale.com> if
  there has not been any prior contact.

## Merge Requests and Code Review

Please, prepare your MR for a review: Be sure to write a summary section and a
focus section. The summary should summarize what your MR is about. The focus
should point the reviewer to the most important code changes.

Also, create gitlab comments for the reviewer. They should guide the
reviewer through the changes, explain your changes and also point out open
questions.

For further good practices have a look at
[our review guidelines](https://gitlab.com/caosdb/caosdb/-/blob/dev/REVIEW_GUIDELINES.md).
