Legacy code
===========

- Older GCC compilers (before gcc-9) need explicit linking against libstdc++fs.a.  The related
  specifications can be removed from the CMakeLists.txt files once no such compilers are expected in
  the wild anymore.
