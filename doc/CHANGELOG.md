# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2024-11-15
(Joscha Schmiedt)

### Added

- `Connection::ListUsers` method
- Added experimental support for compilation with vcpkg.

### Changed

- Renamed the project to linkahead-cpplib
- Updated dependency versions (Boost, gRPC)
- Updated Python dependencies for Conan 2 and support Python 3.12.
- Updated C++ package manager Conan to 2.5.0 including migration of conanfile.py

### Deprecated

### Removed

### Fixed

- Values can now hold empty vectors and do not silently convert them into a scalar.
- [indiscale#34](https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib/-/issues/34)
  Building shared libraries on Windows.

### Security

### Documentation


## [0.2.2] - 2023-06-15
(Timm Fitschen)

### Added

* Support for `SELECT` queries.

## [0.2.1] - 2022-09-22
(Daniel Hornung)

### Changed ###

* Updated dependencies
* Simplified implementation code a bit

## [0.2.0] - 2022-07-13
(Timm Fitschen)

### Added

* Simple `User` class and methods for user creation/retrieval/deletion.
* Added more variants for the `operator<<` of `LoggerOutputStream` (for `bool`
  and `std::endl`).

### Changed

* Transaction::ExecuteAsynchronously is actually asynchronous now.
* Removed boost from the headers. Boost is only a build dependency from now on.

### Fixed

* Calling "GetFileDescriptor().wrapped->path()" after retrieval leads to SegFault.
* #41 Updated Conan version in CI pipeline.

## [0.1.2] - 2022-05-31
(Florian Spreckelsen)

### Fixed

* #41 Updated Conan version in CI pipeline.

## [0.1.1 - 2022-04-12]

### Security

* Bumped zlib dependency to 1.2.12.

## [0.1.0 - 2021-12-11]

Initial Release.

Please review [FEATURES.md](./FEATURES.md) for an overview of the features of
this library. Apart from that, the API documentation for
[C++](https://docs.indiscale.com/caosdb-cpplib/cppapi/index.html) and [plain
old C](https://docs.indiscale.com/caosdb-cpplib/capi/index.html) specify the
public API.

Note that this is still a pre-stable release as indicated by the major version
number. The API is still under development and is likely to change.
