# liblinkahead

## Welcome

This is the development repository of the LinkAhead C++ Client Library
for LinkAhead and a part of the LinkAhead project.

## Setup

Please read the installation notes [online](https://docs.indiscale.com/caosdb-cpplib/Install_develop.html) or [in the source repository](doc/Install_develop.rst) for instructions
on how to build, use and develop this code.

## Further Reading

Please refer to the [official documentation](https://docs.indiscale.com/caosdb-cpplib/) for more information.

## Contributing

Thank you very much to all contributers—[past,
present](https://gitlab.com/linkahead/linkahead/-/blob/main/HUMANS.md), and prospective
ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](https://gitlab.com/linkahead/linkahead/-/blob/main/CODE_OF_CONDUCT.md).

### How to Contribute

* You found a bug, have a question, or want to request a feature? Please 
[create an issue](https://gitlab.com/linkahead/linkahead-cpplib/-/issues).
* You want to contribute code?
    * **Forking:** Please fork the repository and create a merge request in GitLab and choose this repository as
      target. Make sure to select "Allow commits from members who can merge the target branch" under
      Contribution when creating the merge request. This allows our team to work with you on your
      request.
* You can also  join the LinkAhead community on
  [#linkahead:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org).

## License

* Copyright (C) 2022-2024 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General Public
License](LICENSE.md) (version 3 or later).
