# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# This Makefile is a wrapper for several other scripts.


CLANG_FORMAT ?= clang-format-16
CONAN_SETTINGS = "compiler.libcxx=libstdc++11"

# OS specific handling, with code by Ken Jackson and oHo,
# from https://stackoverflow.com/a/52062069/232888 and
# https://stackoverflow.com/a/14777895/232888
ifeq '$(findstring ;,$(PATH))' ';'
	DETECTED_OS := Windows
else
	DETECTED_OS := $(shell uname 2>/dev/null || echo Unknown)
	DETECTED_OS := $(patsubst CYGWIN%,Cygwin,$(DETECTED_OS))
	DETECTED_OS := $(patsubst MSYS%,MSYS,$(DETECTED_OS))
	DETECTED_OS := $(patsubst MINGW%,MSYS,$(DETECTED_OS))
endif
ifeq ($(DETECTED_OS),Darwin)  # Test if we are on MacOS
	CONAN_SETTINGS := "compiler.cppstd=17"
endif


.PHONY: help
help:
	@echo "Targets:"
	@echo "    style - auto-format the source files."
	@echo "    conan-install-deps - Install dependencies locally with Conan."
	@echo -e "    conan-create - Create Conan binary package in the local conan\n"\
	"                   repostory."
	@echo "    conan - Install dependencies, then build and install package."
	@echo "    doc - Generate documentation."
	@echo "    conan-debug - Install dependencies and create Conan binary package in Debug mode."
	@echo "    conan-build - Build the Conan package."
	@echo "    conan-build-debug - Build the Conan package in Debug mode."
	@echo "    vcpkg-install - Install dependencies with Vcpkg."
	@echo "    vcpkg-build-release - Build the project with Vcpkg in Release mode."
	@echo "    test-debug - Install dependencies, build the project in Debug mode, and run the tests."
	@echo "    test-release - Install dependencies, build the project in Release mode, and run the tests."
	@echo "    clean - Remove build directory."

style:
	$(CLANG_FORMAT) -i --verbose \
	 $$(find test/ src/ include/ -type f -iname "*.cpp" -o -iname "*.h" -o -iname "*.h.in")
.PHONY: style

conan-install-deps:
	conan install . -s $(CONAN_SETTINGS) -s build_type=Release || \
	  (echo "'conan install' failed, trying to build from sources..."; \
	   conan install . -s $(CONAN_SETTINGS) -s build_type=Release --build=missing)
.PHONY: conan-install-deps

conan-install-debug:
	conan install . -s $(CONAN_SETTINGS) -s build_type=Debug || \
	  (echo "'conan install' failed, trying to build from sources..."; \
	   conan install . -s $(CONAN_SETTINGS) -s build_type=Debug --build=missing)
.PHONY: conan-install-debug

conan-create:
	conan create -s $(CONAN_SETTINGS) -o linkahead/*:build_acm=True . 
.PHONY: conan-create

conan-create-debug:
	conan create -s $(CONAN_SETTINGS) -s build_type=Debug -o linkahead/*:build_acm=True .

.PHONY: conan-create-debug

conan: conan-install-deps conan-create
.PHONY: conan

doc: conan-install-deps
	@doxygen --version || ( echo "Doxygen not found.  Please install Doxygen first." ; exit 1 )
	cd build/Release\
	  && cmake -S ../.. -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="generators/conan_toolchain.cmake" -DCMAKE_POLICY_DEFAULT_CMP0091="NEW" -DCMAKE_BUILD_TYPE="Release" \
	  && cmake --build . --target doc-sphinx \
	  && echo "The documentation starts at build/Release/doc/sphinx_out/index.html ."
.PHONY: doc

conan-debug: conan-install-debug conan-create-debug
.PHONY: conan-debug

conan-build:
	conan build . -s $(CONAN_SETTINGS) -s build_type=Release
.PHONY: conan-build

conan-build-debug:
	@command -v lcov || (echo "Could not find 'lcov', exiting."; exit 1)
	@command -v clang-tidy || command -v clang-tidy-16 || \
	  (echo "Could not find 'clang-tidy'(-16), exiting."; exit 1)
	conan build . -s $(CONAN_SETTINGS) -s build_type=Debug
.PHONY: conan-build-debug

vcpkg-install:
	./vcpkg/bootstrap-vcpkg.sh -disableMetrics\
      &&vcpkg/vcpkg install
.PHONY: vcpkg-install

vcpkg-build-release: vcpkg-install
	mkdir -p build/build_tools\
	  && cp vcpkg_installed/x64-linux/tools/grpc/grpc_cpp_plugin build/build_tools\
	  && cd build\
	  && cmake -S .. -B . -DCMAKE_TOOLCHAIN_FILE=../vcpkg/scripts/buildsystems/vcpkg.cmake -DVCPKG_TARGET_TRIPLET=x64-linux-release -DCMAKE_BUILD_TYPE=Release\
      && cmake --build .
.PHONY: vcpkg-build-release

ctest-debug:
	cd build/Debug && ctest
.PHONY: ctest-debug

ctest-release:
	cd build/Release && ctest
.PHONY: ctest-release

test-debug: conan-install-debug conan-build-debug ctest-debug
.PHONY: test-debug

test-release: conan-install-deps conan-build ctest-release
.PHONY: test-release

clean:
	rm -rf build
.PHONY: clean
