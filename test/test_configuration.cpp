/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <gtest/gtest.h>             // for Test, TestPartResult, TestPartR...
#include <gtest/gtest-message.h>     // for Message
#include <gtest/gtest-test-part.h>   // for TestPartResult
#include <gtest/gtest_pred_impl.h>   // for TestInfo, TEST_F, Test
#include <filesystem>                // for path
#include <memory>                    // for make_shared, shared_ptr
#include <string>                    // for basic_string, operator+
#include "linkahead/configuration.h" // for ConfigurationError, Configurati...
#include "linkahead/exceptions.h"    // for ConfigurationError
#include "linkahead/log_level.h"     // for LINKAHEAD_DEFAULT_LOG_LEVEL
#include "linkahead/logging.h"       // for ConsoleSinkConfiguration, Loggi...
#include "linkahead_test_utility.h"  // for EXPECT_THROW_MESSAGE, TEST_DATA...

namespace linkahead::configuration {

class test_configuration : public ::testing::Test {
protected:
  void SetUp() override { ConfigurationManager::Clear(); }
  void TearDown() override { ConfigurationManager::Clear(); }
};

TEST_F(test_configuration, load_json) {
  ConfigurationManager::LoadSingleJSONConfiguration(TEST_DATA_DIR + "/test_linkahead_client.json");
  EXPECT_THROW_MESSAGE(ConfigurationManager::LoadSingleJSONConfiguration("anything"),
                       ConfigurationError, "This LinkAhead client has already been configured.");

  ConfigurationManager::Clear();
  EXPECT_THROW_MESSAGE(ConfigurationManager::LoadSingleJSONConfiguration("anything"),
                       ConfigurationError, "Configuration file does not exist.");
  ConfigurationManager::Clear();
}

TEST_F(test_configuration, get_default_connection_configuration_error) {
  EXPECT_THROW_MESSAGE(ConfigurationManager::GetDefaultConnectionName(), ConfigurationError,
                       "This LinkAhead client has not been configured.");

  ConfigurationManager::LoadSingleJSONConfiguration(
    TEST_DATA_DIR + "/test_broken_linkahead_client_no_connections1.json");
  EXPECT_THROW_MESSAGE(ConfigurationManager::GetDefaultConnectionName(), ConfigurationError,
                       "This LinkAhead client hasn't any configured connections.");
  ConfigurationManager::Clear();

  ConfigurationManager::LoadSingleJSONConfiguration(
    TEST_DATA_DIR + "/test_broken_linkahead_client_no_connections2.json");
  EXPECT_THROW_MESSAGE(ConfigurationManager::GetDefaultConnectionName(), ConfigurationError,
                       "This LinkAhead client hasn't any configured connections.");
  ConfigurationManager::Clear();

  ConfigurationManager::LoadSingleJSONConfiguration(
    TEST_DATA_DIR + "/test_broken_linkahead_client_no_connections3.json");
  EXPECT_THROW_MESSAGE(ConfigurationManager::GetDefaultConnectionName(), ConfigurationError,
                       "This LinkAhead client hasn't any configured connections.");
  ConfigurationManager::Clear();
}

TEST_F(test_configuration, initialize_logging) {
  auto logging_configuration = linkahead::logging::LoggingConfiguration(LINKAHEAD_LOG_LEVEL_ALL);
  auto console_sink = std::make_shared<linkahead::logging::ConsoleSinkConfiguration>(
    "console", LINKAHEAD_DEFAULT_LOG_LEVEL);
  logging_configuration.AddSink(console_sink);

  initialize_logging(logging_configuration);
}

} // namespace linkahead::configuration
