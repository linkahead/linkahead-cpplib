/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <gtest/gtest.h>
#include <cstdint>                    // for int64_t
#include <string>                     // for basic_string, string
#include <vector>                     // for vector
#include "caosdb/entity/v1/main.pb.h" // for AtomicDataType, DataType, Entity
#include "linkahead/data_type.h"      // for AtomicDataType, DataType, List...
#include "linkahead/entity.h"         // for Property, Entity, Properties
#include "linkahead/value.h"          // for ScalarValue, Value

namespace linkahead::entity {
using ProtoEntity = caosdb::entity::v1::Entity;
using ProtoParent = caosdb::entity::v1::Parent;
using ProtoDataType = caosdb::entity::v1::DataType;
using ProtoAtomicDataType = caosdb::entity::v1::AtomicDataType;

TEST(test_list_property, test_list_of_text) {
  Property list_property;
  list_property.SetDataType(DataType::ListOf(AtomicDataType::TEXT));
  std::vector<std::string> in_value{"item1", "item2", "item3"};
  list_property.SetValue(in_value);

  Entity entity;
  entity.SetRole(Role::RECORD_TYPE);
  entity.SetName("TestRT");
  entity.AppendProperty(list_property);

  const auto &data_type = entity.GetProperties().at(0).GetDataType();
  const auto &value = entity.GetProperties().at(0).GetValue();

  EXPECT_TRUE(data_type.IsList());
  EXPECT_TRUE(data_type.GetAsList().IsListOfAtomic());
  EXPECT_EQ(data_type.GetAsList().GetAtomicDataType(), AtomicDataType::TEXT);

  EXPECT_TRUE(value.IsVector());
  EXPECT_EQ(value.GetAsVector().size(), 3);
  EXPECT_TRUE(value.GetAsVector().at(1).IsString());
  EXPECT_EQ(value.GetAsVector().at(1).GetAsString(), "item2");
}

TEST(test_list_property, test_list_reassignment) {
  Property list_property;
  // assign int list
  std::vector<int64_t> int_values{1, 2, 3};
  list_property.SetValue(int_values);
  const auto &value_ints = list_property.GetValue();
  EXPECT_TRUE(value_ints.IsVector());
  EXPECT_EQ(value_ints.GetAsVector().size(), 3);
  for (int ii = 0; ii < 3; ii++) {
    EXPECT_TRUE(value_ints.GetAsVector().at(ii).IsInt64());
    EXPECT_EQ(value_ints.GetAsVector().at(ii).GetAsInt64(), int_values[ii]);
  }

  // Re-assign to double scalar
  double double_value(1.23);
  list_property.SetValue(double_value);
  const auto &value_double = list_property.GetValue();
  EXPECT_FALSE(value_double.IsVector());
  EXPECT_TRUE(value_double.IsDouble());
  EXPECT_FALSE(value_double.IsInt64());
  EXPECT_EQ(value_double.GetAsDouble(), double_value);

  // Re-assign to boolean list
  std::vector<bool> bool_values{true, false, false, true};
  list_property.SetValue(bool_values);
  const auto &value_bools = list_property.GetValue();
  EXPECT_TRUE(value_bools.IsVector());
  EXPECT_EQ(value_bools.GetAsVector().size(), 4);
  for (int jj = 0; jj < 4; jj++) {
    EXPECT_TRUE(value_bools.GetAsVector().at(jj).IsBool());
    EXPECT_FALSE(value_bools.GetAsVector().at(jj).IsInt64());
    EXPECT_EQ(value_bools.GetAsVector().at(jj).GetAsBool(), bool_values[jj]);
  }
}

} // namespace linkahead::entity
