/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <gtest/gtest.h>
#include <map>                                // for _Rb_tree_const_iterator
#include <stdexcept>                          // for out_of_range
#include <string>                             // for basic_string, string
#include <utility>                            // for pair
#include <boost/beast/core/detail/base64.hpp> // for encoded_size
#include "linkahead/data_type.h"              // for AtomicDataType, atomic...
#include "linkahead/entity.h"                 // for Importance, Role, impo...
#include "linkahead/status_code.h"            // for get_status_description
#include "linkahead/utility.h"                // for getEnumValueFromName
#include "linkahead_test_utility.h"           // for EXPECT_THROW_MESSAGE

namespace linkahead::utility {

TEST(test_utility, base64_encode) {
  auto test_plain = std::string("admin:caosdb");
  auto test_encoded = std::string("YWRtaW46Y2Fvc2Ri");
  EXPECT_EQ(12, test_plain.size());
  EXPECT_EQ(16, boost::beast::detail::base64::encoded_size(test_plain.size()));
  EXPECT_EQ(test_encoded, base64_encode(test_plain));
}

TEST(test_utility, enum_names) {
  // All working enums
  for (const auto &entry : linkahead::entity::importance_names) {
    EXPECT_EQ(getEnumNameFromValue<linkahead::entity::Importance>(entry.first), entry.second);
    EXPECT_EQ(getEnumValueFromName<linkahead::entity::Importance>(entry.second), entry.first);
  }
  for (const auto &entry : linkahead::entity::role_names) {
    EXPECT_EQ(getEnumNameFromValue<linkahead::entity::Role>(entry.first), entry.second);
    EXPECT_EQ(getEnumValueFromName<linkahead::entity::Role>(entry.second), entry.first);
  }
  for (const auto &entry : linkahead::entity::atomicdatatype_names) {
    EXPECT_EQ(getEnumNameFromValue<linkahead::entity::AtomicDataType>(entry.first), entry.second);
    EXPECT_EQ(getEnumValueFromName<linkahead::entity::AtomicDataType>(entry.second), entry.first);
  }

  // Some non-working examples
  EXPECT_THROW_MESSAGE(getEnumValueFromName<linkahead::entity::Importance>("Invalid name"),
                       std::out_of_range, "Could not find enum value for string 'Invalid name'.");
}

TEST(test_utility, test_status_code_description) {
  EXPECT_EQ(linkahead::get_status_description(12412323), "MISSING DESCRIPTION");
  EXPECT_EQ(linkahead::get_status_description(static_cast<int>(StatusCode::UNKNOWN)),
            "Unknown error. This is typically a bug (server or client).");
}

} // namespace linkahead::utility
