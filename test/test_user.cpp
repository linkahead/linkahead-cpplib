/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "linkahead/acm/user.h" // for User
#include <gtest/gtest.h>
#include <gtest/gtest-message.h>   // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult, SuiteApiRe...
#include <gtest/gtest_pred_impl.h> // for Test, EXPECT_EQ, TEST
#include <string>                  // for allocator, string
#include <utility>                 // for move

namespace linkahead::acm {

TEST(test_user, user_name) {
  auto user = User("user1");
  EXPECT_EQ(user.GetName(), "user1");
  user.SetName("user2");
  EXPECT_EQ(user.GetName(), "user2");
}

TEST(test_user, user_realm) {
  auto user1 = User("realm1", "user1");
  EXPECT_EQ(user1.GetName(), "user1");
  EXPECT_EQ(user1.GetRealm(), "realm1");
  user1.SetName("user3");
  EXPECT_EQ(user1.GetRealm(), "realm1");

  auto user2 = User("realm1", "user2");
  EXPECT_EQ(user2.GetRealm(), "realm1");
  EXPECT_EQ(user2.GetName(), "user2");
  user2.SetRealm("realm2");
  EXPECT_EQ(user2.GetRealm(), "realm2");
  EXPECT_EQ(user2.GetName(), "user2");
}

TEST(test_user, user_password) {
  auto user1 = User("realm1", "user1");
  user1.SetPassword("1234");
  EXPECT_EQ(user1.GetName(), "user1");
  EXPECT_EQ(user1.GetPassword(), "1234");
  user1.SetName("user3");
  EXPECT_EQ(user1.GetPassword(), "1234");

  auto user2 = User("realm1", "user2");
  user2.SetPassword("1234");
  EXPECT_EQ(user2.GetPassword(), "1234");
  EXPECT_EQ(user2.GetName(), "user2");
  user2.SetPassword("abcd");
  EXPECT_EQ(user2.GetPassword(), "abcd");
  EXPECT_EQ(user2.GetName(), "user2");
}

TEST(test_user, test_copy_constructor) {
  const auto user1 = User("user1");
  User user2(user1);

  EXPECT_EQ(user2.GetName(), "user1");
  EXPECT_EQ(user1.GetName(), "user1");

  user2.SetName("user2");

  EXPECT_EQ(user2.GetName(), "user2");
  EXPECT_EQ(user1.GetName(), "user1");
}

TEST(test_user, test_copy_assign) {
  const auto user1 = User("user1");
  auto user2 = user1;

  EXPECT_EQ(user2.GetName(), "user1");
  user2.SetName("user2");
  EXPECT_EQ(user2.GetName(), "user2");
  EXPECT_EQ(user1.GetName(), "user1");
}

TEST(test_user, test_move_constructor) {
  auto user1 = User("user1");
  User user2(std::move(user1));

  EXPECT_EQ(user2.GetName(), "user1");
  // NOLINTNEXTLINE
  EXPECT_EQ(user1.GetName(), "");
}

TEST(test_user, test_move_assign) {
  auto user1 = User("user2");
  user1.SetPassword("1234");
  EXPECT_EQ(user1.GetName(), "user2");
  EXPECT_EQ(user1.GetPassword(), "1234");

  User user2;
  user2 = std::move(user1);

  EXPECT_EQ(user2.GetName(), "user2");
  EXPECT_EQ(user2.GetPassword(), "1234");
  // NOLINTNEXTLINE
  EXPECT_EQ(user1.GetName(), "");
  // NOLINTNEXTLINE
  EXPECT_EQ(user1.GetPassword(), "");
}

} // namespace linkahead::acm
