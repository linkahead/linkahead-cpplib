/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>
#include <filesystem>                                // for path, exists
#include <string>                                    // for basic_string
#include "linkahead/file_transmission/file_reader.h" // for FileReader
#include "linkahead/file_transmission/file_writer.h" // for FileWriter
namespace fs = std::filesystem;

namespace linkahead::transaction {

class test_file_transmission : public ::testing::Test {
protected:
  fs::path test_file_name;

  void SetUp() override { test_file_name = fs::path("this_is_a_test_file_remove_me.dat"); }

  void TearDown() override { fs::remove(test_file_name); }
};

TEST_F(test_file_transmission, test_file_writer_reader) {
  ASSERT_FALSE(fs::exists(test_file_name));

  {
    FileWriter writer(test_file_name);
    std::string buffer_out(1024, 'c');
    for (int i = 0; i < 8; i++) {
      writer.write(buffer_out);
    }
  }
  EXPECT_EQ(fs::file_size(test_file_name), 1024 * 8);

  {
    FileReader reader(test_file_name);
    std::string buffer_in(1024, '\0');
    for (int i = 0; i < 8; i++) {
      reader.read(buffer_in);
      EXPECT_EQ(buffer_in, std::string(1024, 'c'));
    }
  }
}

} // namespace linkahead::transaction
