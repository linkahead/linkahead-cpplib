/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <gtest/gtest.h>
#include "caosdb/info/v1/main.pb.h" // for VersionInfo
#include "gtest/gtest_pred_impl.h"  // for Test, EXPECT_EQ, TestInfo, TEST
#include "linkahead/info.h"         // for VersionInfo

namespace linkahead::info {
using ProtoVersionInfo = caosdb::info::v1::VersionInfo;

TEST(test_info, create_info_from_proto_info) {
  auto *origial = new ProtoVersionInfo();
  origial->set_major(12);
  origial->set_patch(56);
  origial->set_pre_release("SNAPSHOT");
  origial->set_build("1234asdf");

  VersionInfo wrapper(origial);

  EXPECT_EQ(12, wrapper.GetMajor());
  EXPECT_EQ(0, wrapper.GetMinor()); // default value.
  EXPECT_EQ(56, wrapper.GetPatch());
  EXPECT_EQ("SNAPSHOT", wrapper.GetPreRelease());
  EXPECT_EQ("1234asdf", wrapper.GetBuild());
}

} // namespace linkahead::info
