/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 */
#include "clinkahead.h"
#include "linkahead/connection.h"
#include "linkahead/constants.h"
#include "linkahead/data_type.h" // for DataType, AtomicDat...
#include "linkahead/entity.h"
#include "linkahead/value.h"
#include "linkahead/utility.h"
#include "linkahead/status_code.h"
#include "linkahead/logging.h"
#include <cassert>
#include <cstring>
#include <exception>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>

extern "C" {

#define CLINKAHEAD_LOGGER_NAME "clinkahead"

#define WRAPPED_ENTITY_CAST(name) static_cast<linkahead::entity::Entity *>(name->wrapped_entity)

#define WRAPPED_PROPERTY_CAST(name)                                                                \
  static_cast<linkahead::entity::Property *>(name->wrapped_property)

#define WRAPPED_PARENT_CAST(name) static_cast<linkahead::entity::Parent *>(name->wrapped_parent)

#define WRAPPED_MESSAGE_CAST(name) static_cast<linkahead::entity::Message *>(name->wrapped_message)

#define WRAPPED_DATATYPE_CAST(name)                                                                \
  static_cast<linkahead::entity::DataType *>(name->wrapped_datatype)

#define WRAPPED_VALUE_CAST(name)                                                                   \
  static_cast<linkahead::entity::AbstractValue *>(name->wrapped_value)

#define ENUM_NAME_FROM_VALUE(arg, etype)                                                           \
  linkahead::utility::getEnumNameFromValue<linkahead::entity::etype>(arg)

#define ENUM_VALUE_FROM_NAME(arg, etype)                                                           \
  linkahead::utility::getEnumValueFromName<linkahead::entity::etype>(arg)

/*
 * Macro for wrapping every function into a try-catch clause. If an exception
 * occurs, the given StatusCode is being returned.
 */
#define ERROR_RETURN_CODE(code, fun, body)                                                         \
  fun {                                                                                            \
    LINKAHEAD_LOG_TRACE(CLINKAHEAD_LOGGER_NAME) << "Enter " << #fun;                               \
    try {                                                                                          \
      body                                                                                         \
    } catch (const std::exception &exc) {                                                          \
      linkahead::logging::linkahead_log_fatal(CLINKAHEAD_LOGGER_NAME, exc.what());                 \
      return linkahead::StatusCode::code;                                                          \
    }                                                                                              \
  }

/**
 * Macro for entity getters
 */
#define LINKAHEAD_ENTITY_GET(element, GetFunction)                                                 \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_entity_get_##element(linkahead_entity_entity *entity, char **out), {      \
      auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);                                          \
      auto *tmp = (char *)malloc(sizeof(char) * wrapped_entity->GetFunction.length() + 1);         \
      strcpy(tmp, wrapped_entity->GetFunction.c_str());                                            \
      delete[] *out;                                                                               \
      *out = tmp;                                                                                  \
      return 0;                                                                                    \
    })

/**
 * Macro for entity setters
 */
#define LINKAHEAD_ENTITY_SET(element, value, body_part)                                            \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_entity_set_##element(linkahead_entity_entity *entity, const char *value), \
    {                                                                                              \
      auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);                                          \
      body_part return 0;                                                                          \
    })

/**
 * Macro for property getters
 */
#define LINKAHEAD_PROPERTY_GET(element, GetFunction)                                               \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_property_get_##element(linkahead_entity_property *property, char **out),  \
    {                                                                                              \
      auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);                                    \
      auto *tmp = (char *)malloc(sizeof(char) * wrapped_property->GetFunction.length() + 1);       \
      strcpy(tmp, wrapped_property->GetFunction.c_str());                                          \
      delete[] *out;                                                                               \
      *out = tmp;                                                                                  \
      return 0;                                                                                    \
    })

/**
 * Macro for property setters
 */
#define LINKAHEAD_PROPERTY_SET(element, value, body_part)                                          \
  ERROR_RETURN_CODE(GENERIC_ERROR,                                                                 \
                    int linkahead_entity_property_set_##element(                                   \
                      linkahead_entity_property *property, const char *value),                     \
                    {                                                                              \
                      auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);                    \
                      body_part return 0;                                                          \
                    })

/**
 * Macro for parent getters
 */
#define LINKAHEAD_PARENT_GET(element, GetFunction)                                                 \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_parent_get_##element(linkahead_entity_parent *parent, char **out), {      \
      auto *wrapped_parent = WRAPPED_PARENT_CAST(parent);                                          \
      auto *tmp = (char *)malloc(sizeof(char) * wrapped_parent->GetFunction.length() + 1);         \
      strcpy(tmp, wrapped_parent->GetFunction.c_str());                                            \
      delete[] *out;                                                                               \
      *out = tmp;                                                                                  \
      return 0;                                                                                    \
    })

/**
 * Macro for parent setters
 */
#define LINKAHEAD_PARENT_SET(element, value, body_part)                                            \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_parent_set_##element(linkahead_entity_parent *parent, const char *value), \
    {                                                                                              \
      auto *wrapped_parent = WRAPPED_PARENT_CAST(parent);                                          \
      body_part return 0;                                                                          \
    })

/**
 * Macro for scalar value creators
 */
#define CREATE_VALUE(fname, arg)                                                                   \
  ERROR_RETURN_CODE(GENERIC_ERROR,                                                                 \
                    int linkahead_entity_create_##fname(linkahead_entity_value *out, arg), {       \
                      out->wrapped_value = new linkahead::entity::Value(value);                    \
                      out->_deletable = true;                                                      \
                      return 0;                                                                    \
                    })
/**
 * Macro for list-value creators
 */
#define CREATE_VECTOR_VALUE(fname, type, arg, assign)                                              \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_create_##fname(linkahead_entity_value *out, arg, const int length), {     \
      std::vector<type> value_vec;                                                                 \
      for (int i = 0; i < length; i++) {                                                           \
        value_vec.push_back(assign);                                                               \
      }                                                                                            \
      out->wrapped_value = new linkahead::entity::Value(value_vec);                                \
      out->_deletable = true;                                                                      \
      return 0;                                                                                    \
    })

/**
 * Macro for value is-a functions
 */
#define VALUE_IS(fname, isfunction)                                                                \
  ERROR_RETURN_CODE(                                                                               \
    GENERIC_ERROR,                                                                                 \
    int linkahead_entity_value_is_##fname(linkahead_entity_value *value, bool *out), {             \
      auto *wrapped_value = WRAPPED_VALUE_CAST(value);                                             \
      *out = wrapped_value->isfunction();                                                          \
      return 0;                                                                                    \
    })

/**
 * Macro for some value getters
 */
#define VALUE_GET_AS(fname, getfunction, arg)                                                      \
  ERROR_RETURN_CODE(GENERIC_ERROR,                                                                 \
                    int linkahead_entity_value_get_as_##fname(linkahead_entity_value *value, arg), \
                    {                                                                              \
                      auto *wrapped_value = WRAPPED_VALUE_CAST(value);                             \
                      *out = wrapped_value->getfunction();                                         \
                      return 0;                                                                    \
                    })

int linkahead_constants_LIBLINKAHEAD_VERSION_MAJOR() {
  return linkahead::LIBLINKAHEAD_VERSION_MAJOR;
}

int linkahead_constants_LIBLINKAHEAD_VERSION_MINOR() {
  return linkahead::LIBLINKAHEAD_VERSION_MINOR;
}

int linkahead_constants_LIBLINKAHEAD_VERSION_PATCH() {
  return linkahead::LIBLINKAHEAD_VERSION_PATCH;
}

int linkahead_constants_COMPATIBLE_SERVER_VERSION_MAJOR() {
  return linkahead::COMPATIBLE_SERVER_VERSION_MAJOR;
}

int linkahead_constants_COMPATIBLE_SERVER_VERSION_MINOR() {
  return linkahead::COMPATIBLE_SERVER_VERSION_MINOR;
}

int linkahead_constants_COMPATIBLE_SERVER_VERSION_PATCH() {
  return linkahead::COMPATIBLE_SERVER_VERSION_PATCH;
}

const char *linkahead_constants_COMPATIBLE_SERVER_VERSION_PRE_RELEASE() {
  return linkahead::COMPATIBLE_SERVER_VERSION_PRE_RELEASE;
}

int linkahead_status_code_OTHER_CLIENT_ERROR() { return linkahead::StatusCode::OTHER_CLIENT_ERROR; }

const char *linkahead_utility_get_env_fallback(const char *name, const char *fallback) {
  return linkahead::utility::get_env_fallback(name, fallback);
}

const char *linkahead_get_status_description(int code) {
  return linkahead::get_status_description(code).c_str();
}

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_create_pem_file_certificate_provider(
                    linkahead_connection_certificate_provider *out, const char *path),
                  {
                    out->wrapped_certificate_provider =
                      new linkahead::configuration::PemFileCertificateProvider(std::string(path));
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_delete_certificate_provider(
                    linkahead_connection_certificate_provider *provider),
                  {
                    if (provider->_deletable && provider->wrapped_certificate_provider) {
                      delete static_cast<linkahead::configuration::CertificateProvider *>(
                        provider->wrapped_certificate_provider);
                    }
                    provider->wrapped_certificate_provider = nullptr;
                    provider->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_authentication_create_plain_password_authenticator(
                    linkahead_authentication_authenticator *out, const char *username,
                    const char *password),
                  {
                    out->wrapped_authenticator =
                      new linkahead::authentication::PlainPasswordAuthenticator(
                        std::string(username), std::string(password));
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_authentication_delete_authenticator(
                    linkahead_authentication_authenticator *authenticator),
                  {
                    if (authenticator->_deletable && authenticator->wrapped_authenticator) {
                      delete static_cast<linkahead::authentication::Authenticator *>(
                        authenticator->wrapped_authenticator);
                    }
                    authenticator->wrapped_authenticator = nullptr;
                    authenticator->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_connection_create_tls_connection_configuration(
    linkahead_connection_connection_configuration *out, const char *host, const int port,
    linkahead_authentication_authenticator *authenticator,
    linkahead_connection_certificate_provider *provider),
  {
    auto host_str = std::string(host);
    if (authenticator != nullptr && provider != nullptr) {
      auto wrapped_provider = static_cast<linkahead::configuration::CertificateProvider *>(
        provider->wrapped_certificate_provider);
      auto wrapped_authenticator = static_cast<linkahead::authentication::Authenticator *>(
        authenticator->wrapped_authenticator);
      out->wrapped_connection_configuration =
        new linkahead::configuration::TlsConnectionConfiguration(host_str, port, *wrapped_provider,
                                                                 *wrapped_authenticator);
    } else if (authenticator != nullptr) {
      auto wrapped_authenticator = static_cast<linkahead::authentication::Authenticator *>(
        authenticator->wrapped_authenticator);
      out->wrapped_connection_configuration =
        new linkahead::configuration::TlsConnectionConfiguration(host_str, port,
                                                                 *wrapped_authenticator);
    } else if (provider != nullptr) {
      auto wrapped_provider = static_cast<linkahead::configuration::CertificateProvider *>(
        provider->wrapped_certificate_provider);
      out->wrapped_connection_configuration =
        new linkahead::configuration::TlsConnectionConfiguration(host_str, port, *wrapped_provider);
    } else {
      out->wrapped_connection_configuration =
        new linkahead::configuration::TlsConnectionConfiguration(host_str, port);
    }
    out->_deletable = true;
    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_create_insecure_connection_configuration(
                    linkahead_connection_connection_configuration *out, const char *host,
                    const int port),
                  {
                    out->wrapped_connection_configuration =
                      new linkahead::configuration::InsecureConnectionConfiguration(host, port);
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_delete_connection_configuration(
                    linkahead_connection_connection_configuration *configuration),
                  {
                    if (configuration->_deletable &&
                        configuration->wrapped_connection_configuration) {
                      delete static_cast<linkahead::configuration::ConnectionConfiguration *>(
                        configuration->wrapped_connection_configuration);
                    }
                    configuration->wrapped_connection_configuration = nullptr;
                    configuration->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_create_connection(
                    linkahead_connection_connection *out,
                    const linkahead_connection_connection_configuration *configuration),
                  {
                    linkahead::configuration::ConnectionConfiguration *config =
                      static_cast<linkahead::configuration::ConnectionConfiguration *>(
                        configuration->wrapped_connection_configuration);
                    out->wrapped_connection = new linkahead::connection::Connection(*config);
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_connection_delete_connection(linkahead_connection_connection *connection), {
    if (connection->_deletable && connection->wrapped_connection) {
      delete static_cast<linkahead::connection::Connection *>(connection->wrapped_connection);
    }
    connection->wrapped_connection = nullptr;
    connection->_deletable = false;
    return 0;
  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_connection_get_version_info(linkahead_info_version_info *out,
                                            const linkahead_connection_connection *connection),
  {
    auto *wrapped_connection =
      static_cast<linkahead::connection::Connection *>(connection->wrapped_connection);

    auto status = wrapped_connection->RetrieveVersionInfoNoExceptions();
    if (status.IsError()) {
      return status.GetCode();
    }
    auto version_info = wrapped_connection->GetVersionInfo();

    out->major = (int)version_info->GetMajor();
    out->minor = (int)version_info->GetMinor();
    out->patch = (int)version_info->GetPatch();

    // copy pre_release, needs local variable because out->pre_release is const
    auto *pre_release = (char *)malloc(sizeof(char) * (version_info->GetPreRelease().length() + 1));
    strcpy(pre_release, version_info->GetPreRelease().c_str());
    out->pre_release = pre_release;

    // copy build, needs local variable because out->build is const
    auto *build = (char *)malloc(sizeof(char) * (version_info->GetBuild().length() + 1));
    strcpy(build, version_info->GetBuild().c_str());
    out->build = build;

    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_connection_manager_get_default_connection(
                    linkahead_connection_connection *out),
                  {
                    out->wrapped_connection =
                      linkahead::connection::ConnectionManager::GetDefaultConnection().get();
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_connection_connection_manager_get_connection(linkahead_connection_connection *out,
                                                             const char *name),
  {
    out->wrapped_connection =
      linkahead::connection::ConnectionManager::GetConnection(std::string(name)).get();
    // managed by the connection manager now, so not
    // to be deleted manually
    out->_deletable = false;
    return 0;
  })

/****************************************************************************
 * ENTITY STUFF AND TRANSACTIONS
 ****************************************************************************/
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_connection_connection_create_transaction(
                    linkahead_connection_connection *connection,
                    linkahead_transaction_transaction *out),
                  {
                    auto *wrapped_connection = static_cast<linkahead::connection::Connection *>(
                      connection->wrapped_connection);
                    out->wrapped_transaction = wrapped_connection->CreateTransaction().release();
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_delete_transaction(linkahead_transaction_transaction *transaction), {
    if (transaction->_deletable && transaction->wrapped_transaction) {
      delete static_cast<linkahead::transaction::Transaction *>(transaction->wrapped_transaction);
    }
    transaction->wrapped_transaction = nullptr;
    transaction->_deletable = false;
    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_retrieve_by_id(
                    linkahead_transaction_transaction *transaction, const char *id),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    return wrapped_transaction->RetrieveById(std::string(id));
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_transaction_retrieve_and_download_file_by_id(
    linkahead_transaction_transaction *transaction, const char *id, const char *path),
  {
    auto *wrapped_transaction =
      static_cast<linkahead::transaction::Transaction *>(transaction->wrapped_transaction);
    return wrapped_transaction->RetrieveAndDownloadFileById(std::string(id), std::string(path));
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_retrieve_by_ids(
                    linkahead_transaction_transaction *transaction, const char *ids[], int length),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    return wrapped_transaction->RetrieveById(ids, ids + length);
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_query(
                    linkahead_transaction_transaction *transaction, const char *query),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    return wrapped_transaction->Query(std::string(query));
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_insert_entity(
                    linkahead_transaction_transaction *transaction,
                    linkahead_entity_entity *entity),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);

                    return wrapped_transaction->InsertEntity(wrapped_entity);
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_update_entity(
                    linkahead_transaction_transaction *transaction,
                    linkahead_entity_entity *entity),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);

                    return wrapped_transaction->UpdateEntity(wrapped_entity);
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_delete_by_id(
                    linkahead_transaction_transaction *transaction, const char *id),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);

                    return wrapped_transaction->DeleteById(std::string(id));
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_transaction_execute(linkahead_transaction_transaction *transaction), {
    auto *wrapped_transaction =
      static_cast<linkahead::transaction::Transaction *>(transaction->wrapped_transaction);
    wrapped_transaction->ExecuteAsynchronously();
    auto status = wrapped_transaction->WaitForIt();
    return status.GetCode();
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_get_result_set(
                    linkahead_transaction_transaction *transaction,
                    linkahead_transaction_result_set *out),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    out->wrapped_result_set = (void *)(&(wrapped_transaction->GetResultSet()));
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_release_result_set(
                    linkahead_transaction_transaction *transaction,
                    linkahead_transaction_result_set *out),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    out->wrapped_result_set = (void *)(wrapped_transaction->ReleaseResultSet());
                    // out is the owner now, that are the semantics of ReleaseResultSet
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_delete_result_set(linkahead_transaction_result_set *result_set), {
    if (result_set->_deletable && result_set->wrapped_result_set) {
      delete static_cast<linkahead::entity::Entity *>(result_set->wrapped_result_set);
    }
    result_set->wrapped_result_set = nullptr;
    result_set->_deletable = false;
    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_transaction_transaction_get_count_result(
                    linkahead_transaction_transaction *transaction, long *out),
                  {
                    auto *wrapped_transaction = static_cast<linkahead::transaction::Transaction *>(
                      transaction->wrapped_transaction);
                    long cr(wrapped_transaction->GetCountResult());
                    *out = cr;
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_result_set_at(linkahead_transaction_result_set *result_set,
                                          linkahead_entity_entity *entity, int index),
  {
    auto *wrapped_result_set =
      static_cast<linkahead::transaction::MultiResultSet *>(result_set->wrapped_result_set);
    entity->wrapped_entity = wrapped_result_set->mutable_at(index);
    return 0;
  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_result_set_release_at(linkahead_transaction_result_set *result_set,
                                                  linkahead_entity_entity *entity, int index),
  {
    auto *wrapped_result_set =
      static_cast<linkahead::transaction::MultiResultSet *>(result_set->wrapped_result_set);
    entity->wrapped_entity = wrapped_result_set->release_at(index);
    // entity is the owner now. That are the semantics of release_at.
    entity->_deletable = true;
    return 0;
  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_transaction_result_set_size(linkahead_transaction_result_set *result_set, int *out),
  {
    auto *wrapped_result_set =
      static_cast<linkahead::transaction::MultiResultSet *>(result_set->wrapped_result_set);
    int size(wrapped_result_set->size());
    *out = size;
    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR, int linkahead_entity_create_entity(linkahead_entity_entity *out), {
  out->wrapped_entity = new linkahead::entity::Entity();
  out->_deletable = true;
  return 0;
})

ERROR_RETURN_CODE(GENERIC_ERROR, int linkahead_entity_delete_entity(linkahead_entity_entity *out), {
  if (out->_deletable && out->wrapped_entity) {
    delete static_cast<linkahead::entity::Entity *>(out->wrapped_entity);
  }
  out->wrapped_entity = nullptr;
  out->_deletable = false;
  return 0;
})

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_create_property(linkahead_entity_property *out), {
                    out->wrapped_property = new linkahead::entity::Property();
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_delete_property(linkahead_entity_property *out), {
                    if (out->_deletable && out->wrapped_property) {
                      delete static_cast<linkahead::entity::Property *>(out->wrapped_property);
                    }
                    out->wrapped_property = nullptr;
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR, int linkahead_entity_create_parent(linkahead_entity_parent *out), {
  out->wrapped_parent = new linkahead::entity::Parent();
  out->_deletable = true;
  return 0;
})

ERROR_RETURN_CODE(GENERIC_ERROR, int linkahead_entity_delete_parent(linkahead_entity_parent *out), {
  if (out->_deletable && out->wrapped_parent) {
    delete static_cast<linkahead::entity::Parent *>(out->wrapped_parent);
  }
  out->wrapped_parent = nullptr;
  out->_deletable = false;
  return 0;
})

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_create_atomic_datatype(linkahead_entity_datatype *out,
                                                              const char *name),
                  {
                    try {
                      auto enum_value = ENUM_VALUE_FROM_NAME(std::string(name), AtomicDataType);
                      out->wrapped_datatype = new linkahead::entity::DataType(enum_value);
                      out->_deletable = true;
                      return 0;
                    } catch (const std::out_of_range &exc) {
                      linkahead::logging::linkahead_log_fatal(CLINKAHEAD_LOGGER_NAME, exc.what());
                      return linkahead::StatusCode::ENUM_MAPPING_ERROR;
                    }
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_create_reference_datatype(linkahead_entity_datatype *out,
                                                                 const char *name),
                  {
                    out->wrapped_datatype = new linkahead::entity::DataType(std::string(name));
                    out->_deletable = true;
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_create_atomic_list_datatype(linkahead_entity_datatype *out,
                                                                   const char *name),
                  {
                    try {
                      auto enum_value = ENUM_VALUE_FROM_NAME(std::string(name), AtomicDataType);
                      out->wrapped_datatype = new linkahead::entity::DataType(
                        linkahead::entity::DataType::ListOf(enum_value));
                      out->_deletable = true;
                      return 0;
                    } catch (const std::out_of_range &exc) {
                      linkahead::logging::linkahead_log_fatal(CLINKAHEAD_LOGGER_NAME, exc.what());
                      return linkahead::StatusCode::ENUM_MAPPING_ERROR;
                    }
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_create_reference_list_datatype(
                    linkahead_entity_datatype *out, const char *name),
                  {
                    out->wrapped_datatype = new linkahead::entity::DataType(
                      linkahead::entity::DataType::ListOf(std::string(name)));
                    out->_deletable = true;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_delete_datatype(linkahead_entity_datatype *out), {
                    if (out->_deletable && out->wrapped_datatype) {
                      delete WRAPPED_DATATYPE_CAST(out);
                    }
                    out->wrapped_datatype = nullptr;
                    out->_deletable = false;
                    return 0;
                  })

CREATE_VALUE(int_value, const int64_t value)
CREATE_VALUE(string_value, const char *value)
CREATE_VALUE(double_value, const double value)
CREATE_VALUE(bool_value, const bool value)
CREATE_VECTOR_VALUE(int_vector_value, int64_t, const int64_t *value, value[i])
CREATE_VECTOR_VALUE(string_vector_value, std::string, const char **value, std::string(value[i]))
CREATE_VECTOR_VALUE(double_vector_value, double, const double *value, value[i])
CREATE_VECTOR_VALUE(bool_vector_value, bool, const bool *value, value[i])

ERROR_RETURN_CODE(GENERIC_ERROR, int linkahead_entity_delete_value(linkahead_entity_value *out), {
  if (out->_deletable && out->wrapped_value) {
    delete WRAPPED_VALUE_CAST(out);
  }
  out->wrapped_value = nullptr;
  out->_deletable = false;
  return 0;
})

LINKAHEAD_ENTITY_GET(id, GetId())
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_role(linkahead_entity_entity *entity, char **out),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    std::string role_str = ENUM_NAME_FROM_VALUE(wrapped_entity->GetRole(), Role);
                    auto *tmp = (char *)malloc(sizeof(char) * role_str.length() + 1);
                    strcpy(tmp, role_str.c_str());
                    delete[] *out;
                    *out = tmp;
                    return 0;
                  })
LINKAHEAD_ENTITY_GET(name, GetName())
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_local_path(linkahead_entity_entity *entity,
                                                             char **out),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    auto path = wrapped_entity->GetLocalPath().string();
                    auto *tmp = (char *)(malloc(sizeof(char) * path.length() + 1));
                    strcpy(tmp, path.c_str());
                    delete[] *out;
                    *out = tmp;
                    return 0;
                  })
// LINKAHEAD_ENTITY_GET(file_path, GetFilePath()) TODO(henrik)
LINKAHEAD_ENTITY_GET(description, GetDescription())
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_datatype(linkahead_entity_entity *entity,
                                                           linkahead_entity_datatype *out),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    out->wrapped_datatype = (void *)(&(wrapped_entity->GetDataType()));
                    out->_deletable = false;
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_value(linkahead_entity_entity *entity,
                                                        linkahead_entity_value *out),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    out->wrapped_value = (void *)(&(wrapped_entity->GetValue()));
                    out->_deletable = false;
                    return 0;
                  })

LINKAHEAD_ENTITY_GET(unit, GetUnit())
LINKAHEAD_ENTITY_GET(version_id, GetVersionId())

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_errors_size(linkahead_entity_entity *entity,
                                                              int *out),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    *out = wrapped_entity->GetErrors().size();
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_error(linkahead_entity_entity *entity,
                                                        linkahead_entity_message *out, int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    out->wrapped_message = wrapped_entity->GetErrors().mutable_at(index);
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_warnings_size(linkahead_entity_entity *entity,
                                                                int *out),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    *out = wrapped_entity->GetWarnings().size();
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_warning(linkahead_entity_entity *entity,
                                                          linkahead_entity_message *out, int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    out->wrapped_message = wrapped_entity->GetWarnings().mutable_at(index);
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_infos_size(linkahead_entity_entity *entity,
                                                             int *out),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    *out = wrapped_entity->GetInfos().size();
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_info(linkahead_entity_entity *entity,
                                                       linkahead_entity_message *out, int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    out->wrapped_message = wrapped_entity->GetInfos().mutable_at(index);
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_properties_size(linkahead_entity_entity *entity,
                                                                  int *out),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    *out = wrapped_entity->GetProperties().size();
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_property(linkahead_entity_entity *entity,
                                                           linkahead_entity_property *out,
                                                           int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    out->wrapped_property = wrapped_entity->GetProperties().mutable_at(index);
                    out->_deletable = false;
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_parents_size(linkahead_entity_entity *entity,
                                                               int *out),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    *out = wrapped_entity->GetParents().size();
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_get_parent(linkahead_entity_entity *entity,
                                                         linkahead_entity_parent *out, int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    out->wrapped_parent = wrapped_entity->GetParents().mutable_at(index);
                    out->_deletable = false;
                    return 0;
                  })

LINKAHEAD_PARENT_GET(id, GetId())
LINKAHEAD_PARENT_GET(name, GetName())
LINKAHEAD_PARENT_GET(description, GetDescription())

LINKAHEAD_PROPERTY_GET(id, GetId())
LINKAHEAD_PROPERTY_GET(name, GetName())
LINKAHEAD_PROPERTY_GET(description, GetDescription())

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_get_importance(linkahead_entity_property *property,
                                                               char **out),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    std::string importance_str =
                      ENUM_NAME_FROM_VALUE(wrapped_property->GetImportance(), Importance);
                    char *tmp = (char *)malloc(sizeof(char) * importance_str.length() + 1);
                    strcpy(tmp, importance_str.c_str());
                    delete[] *out;
                    *out = tmp;
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_get_datatype(linkahead_entity_property *property,
                                                             linkahead_entity_datatype *out),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    out->wrapped_datatype = (void *)(&(wrapped_property->GetDataType()));
                    out->_deletable = false;
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_get_value(linkahead_entity_property *property,
                                                          linkahead_entity_value *out),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    out->wrapped_value = (void *)(&(wrapped_property->GetValue()));
                    out->_deletable = false;
                    return 0;
                  })
LINKAHEAD_PROPERTY_GET(unit, GetUnit())

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_message_get_code(linkahead_entity_message *message,
                                                        int *out),
                  {
                    auto *wrapped_message =
                      static_cast<linkahead::entity::Message *>(message->wrapped_message);
                    *out = wrapped_message->GetCode();
                    return 0;
                  })

ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_entity_message_get_description(linkahead_entity_message *message, char **out), {
    auto *wrapped_message = static_cast<linkahead::entity::Message *>(message->wrapped_message);
    auto *tmp = (char *)malloc(sizeof(char) * wrapped_message->GetDescription().length() + 1);
    strcpy(tmp, wrapped_message->GetDescription().c_str());
    delete[] *out;
    *out = tmp;
    return 0;
  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_datatype_is_undefined(linkahead_entity_datatype *datatype,
                                                             bool *out),
                  {
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
                    *out = wrapped_datatype->IsUndefined();
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_datatype_is_atomic(linkahead_entity_datatype *datatype,
                                                          bool *out),
                  {
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
                    *out = wrapped_datatype->IsAtomic();
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_datatype_is_reference(linkahead_entity_datatype *datatype,
                                                             bool *out),
                  {
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
                    *out = wrapped_datatype->IsReference();
                    return 0;
                  })
ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_entity_datatype_is_list_of_atomic(linkahead_entity_datatype *datatype, bool *out), {
    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
    if (wrapped_datatype->IsList()) {
      const auto &list_datatype = wrapped_datatype->GetAsList();
      *out = list_datatype.IsListOfAtomic();
    } else {
      *out = false;
    }
    return 0;
  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_datatype_is_list_of_reference(
                    linkahead_entity_datatype *datatype, bool *out),
                  {
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
                    if (wrapped_datatype->IsList()) {
                      const auto &list_datatype = wrapped_datatype->GetAsList();
                      *out = list_datatype.IsListOfReference();
                    } else {
                      *out = false;
                    }
                    return 0;
                  })
ERROR_RETURN_CODE(
  GENERIC_ERROR,
  int linkahead_entity_datatype_get_datatype_name(linkahead_entity_datatype *datatype, char **out),
  {
    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);
    std::string datatype_name;
    if (wrapped_datatype->IsList()) {
      const auto &list_datatype = wrapped_datatype->GetAsList();
      if (list_datatype.IsListOfAtomic()) {
        datatype_name = ENUM_NAME_FROM_VALUE(list_datatype.GetAtomicDataType(), AtomicDataType);
      } else {
        datatype_name = list_datatype.GetReferenceDataType().GetName();
      }
    } else {
      if (wrapped_datatype->IsAtomic()) {
        datatype_name = ENUM_NAME_FROM_VALUE(wrapped_datatype->GetAsAtomic(), AtomicDataType);
      } else {
        datatype_name = wrapped_datatype->GetAsReference().GetName();
      }
    }
    char *tmp = (char *)malloc(sizeof(char) * datatype_name.length() + 1);
    strcpy(tmp, datatype_name.c_str());
    delete[] *out;
    *out = tmp;
    return 0;
  })

VALUE_IS(null, IsNull)
VALUE_IS(string, IsString)
VALUE_IS(double, IsDouble)
VALUE_IS(integer, IsInt64)
VALUE_IS(bool, IsBool)
VALUE_IS(vector, IsVector)

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_value_get_as_string(linkahead_entity_value *value,
                                                           char **out),
                  {
                    auto *wrapped_value = WRAPPED_VALUE_CAST(value);
                    auto *tmp =
                      (char *)malloc(sizeof(char) * wrapped_value->GetAsString().length() + 1);
                    strcpy(tmp, wrapped_value->GetAsString().c_str());
                    delete[] *out;
                    *out = tmp;
                    return 0;
                  })
VALUE_GET_AS(double, GetAsDouble, double *out)
VALUE_GET_AS(integer, GetAsInt64, int64_t *out)
VALUE_GET_AS(bool, GetAsBool, bool *out)
VALUE_GET_AS(vector_size, GetAsVector().size, int *out)
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_value_get_as_vector_at(linkahead_entity_value *value,
                                                              linkahead_entity_value *out,
                                                              const int index),
                  {
                    auto *wrapped_value = WRAPPED_VALUE_CAST(value);
                    out->wrapped_value = (void *)(&(wrapped_value->GetAsVector().at(index)));
                    out->_deletable = false;
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_set_role(linkahead_entity_entity *entity,
                                                       const char *role),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    try {
                      auto enum_value = ENUM_VALUE_FROM_NAME(std::string(role), Role);
                      wrapped_entity->SetRole(enum_value);
                      return 0;
                    } catch (const std::out_of_range &exc) {
                      linkahead::logging::linkahead_log_fatal(CLINKAHEAD_LOGGER_NAME, exc.what());
                      return linkahead::StatusCode::ENUM_MAPPING_ERROR;
                    }
                  })
LINKAHEAD_ENTITY_SET(name, name, wrapped_entity->SetName(std::string(name));)
LINKAHEAD_ENTITY_SET(local_path, local_path,
                     return wrapped_entity->SetLocalPath(std::filesystem::path(local_path));)
LINKAHEAD_ENTITY_SET(file_path, file_path, wrapped_entity->SetFilePath(std::string(file_path));)
LINKAHEAD_ENTITY_SET(description, description,
                     wrapped_entity->SetDescription(std::string(description));)
LINKAHEAD_ENTITY_SET(unit, unit, wrapped_entity->SetUnit(std::string(unit));)
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_set_datatype(linkahead_entity_entity *entity,
                                                           linkahead_entity_datatype *datatype),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);

                    return wrapped_entity->SetDataType(*wrapped_datatype);
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_set_value(linkahead_entity_entity *entity,
                                                        linkahead_entity_value *value),
                  {
                    auto *wrapped_entity = WRAPPED_ENTITY_CAST(entity);
                    auto *wrapped_value = WRAPPED_VALUE_CAST(value);

                    return wrapped_entity->SetValue(*wrapped_value);
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_append_parent(linkahead_entity_entity *entity,
                                                            linkahead_entity_parent *parent),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    auto *wrapped_parent =
                      static_cast<linkahead::entity::Parent *>(parent->wrapped_parent);
                    wrapped_entity->AppendParent(*wrapped_parent);
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_remove_parent(linkahead_entity_entity *entity,
                                                            int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    wrapped_entity->RemoveParent(index);
                    return 0;
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_append_property(linkahead_entity_entity *entity,
                                                              linkahead_entity_property *property),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    auto *wrapped_property =
                      static_cast<linkahead::entity::Property *>(property->wrapped_property);
                    wrapped_entity->AppendProperty(*wrapped_property);
                    return 0;
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_entity_remove_property(linkahead_entity_entity *entity,
                                                              int index),
                  {
                    auto *wrapped_entity =
                      static_cast<linkahead::entity::Entity *>(entity->wrapped_entity);
                    wrapped_entity->RemoveProperty(index);
                    return 0;
                  })

LINKAHEAD_PARENT_SET(id, id, wrapped_parent->SetId(std::string(id));)
LINKAHEAD_PARENT_SET(name, name, wrapped_parent->SetName(std::string(name));)

LINKAHEAD_PROPERTY_SET(name, name, wrapped_property->SetName(std::string(name));)
LINKAHEAD_PROPERTY_SET(id, id, wrapped_property->SetId(std::string(id));)
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_set_datatype(linkahead_entity_property *property,
                                                             linkahead_entity_datatype *datatype),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    auto *wrapped_datatype = WRAPPED_DATATYPE_CAST(datatype);

                    return wrapped_property->SetDataType(*wrapped_datatype);
                  })
ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_set_value(linkahead_entity_property *property,
                                                          linkahead_entity_value *value),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    auto *wrapped_value = WRAPPED_VALUE_CAST(value);

                    return wrapped_property->SetValue(*wrapped_value);
                  })

ERROR_RETURN_CODE(GENERIC_ERROR,
                  int linkahead_entity_property_set_importance(linkahead_entity_property *property,
                                                               const char *importance),
                  {
                    auto *wrapped_property = WRAPPED_PROPERTY_CAST(property);
                    try {
                      auto enum_value = ENUM_VALUE_FROM_NAME(std::string(importance), Importance);
                      wrapped_property->SetImportance(enum_value);
                      return 0;
                    } catch (const std::out_of_range &exc) {
                      linkahead::logging::linkahead_log_fatal(CLINKAHEAD_LOGGER_NAME, exc.what());
                      return linkahead::StatusCode::ENUM_MAPPING_ERROR;
                    }
                  })

LINKAHEAD_PROPERTY_SET(unit, unit, wrapped_property->SetUnit(std::string(unit));)
}
