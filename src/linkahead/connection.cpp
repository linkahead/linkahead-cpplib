/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/connection.h"
#ifdef BUILD_ACM
#include "linkahead/acm/user_impl.h"          // for UserImpl
#include "caosdb/acm/v1alpha1/main.grpc.pb.h" // for AccessControlMan...
#include "caosdb/acm/v1alpha1/main.pb.h"      // for CreateSingleUser...
#endif
#include "linkahead/configuration.h"      // for ConnectionConfigur...
#include "linkahead/exceptions.h"         // for ConfigurationError
#include "linkahead/info.h"               // for VersionInfo
#include "caosdb/info/v1/main.grpc.pb.h"  // for GeneralInfoService
#include "caosdb/info/v1/main.pb.h"       // for GetVersionInfoRequest
#include "linkahead/transaction.h"        // for Transaction
#include "linkahead/transaction_status.h" // for TransactionStatus
#include <grpcpp/client_context.h>        // for ClientContext
#include <grpcpp/create_channel.h>        // for CreateChannel
#include <grpcpp/support/status.h>        // for Status
#include <string>                         // for string, operator+
#include <memory>                         // for make_shared
#ifdef BUILD_ACM
#include <vector> // for vector
#endif
// IWYU pragma: no_include "net/proto2/public/repeated_field.h"

namespace linkahead::connection {
#ifdef BUILD_ACM
using caosdb::acm::v1alpha1::AccessControlManagementService;
using caosdb::acm::v1alpha1::CreateSingleUserRequest;
using caosdb::acm::v1alpha1::CreateSingleUserResponse;
using caosdb::acm::v1alpha1::DeleteSingleUserRequest;
using caosdb::acm::v1alpha1::DeleteSingleUserResponse;
using caosdb::acm::v1alpha1::ListUsersRequest;
using caosdb::acm::v1alpha1::ListUsersResponse;
using caosdb::acm::v1alpha1::RetrieveSingleUserRequest;
using caosdb::acm::v1alpha1::RetrieveSingleUserResponse;
using linkahead::acm::UserImpl;
#endif
using caosdb::entity::v1::EntityTransactionService;
using caosdb::entity::v1::FileTransmissionService;
using caosdb::info::v1::GeneralInfoService;
using caosdb::info::v1::GetVersionInfoRequest;
using caosdb::info::v1::GetVersionInfoResponse;
using linkahead::configuration::ConfigurationManager;
using linkahead::configuration::ConnectionConfiguration;
using linkahead::info::VersionInfo;
using linkahead::transaction::Transaction;
using linkahead::transaction::TransactionStatus;

ConnectionManager ConnectionManager::mInstance;

ConnectionManager &ConnectionManager::GetInstance() { return mInstance; }

Connection::Connection(const ConnectionConfiguration &configuration) {
  const std::string target =
    configuration.GetHost() + ":" + std::to_string(configuration.GetPort());
  this->channel = grpc::CreateChannel(target, configuration.GetChannelCredentials());
  this->general_info_service = GeneralInfoService::NewStub(this->channel);
  this->entity_transaction_service =
    std::make_shared<EntityTransactionService::Stub>(this->channel);
  this->file_transmission_service = std::make_shared<FileTransmissionService::Stub>(this->channel);
#ifdef BUILD_ACM
  this->access_controll_management_service =
    std::make_unique<AccessControlManagementService::Stub>(this->channel);
#endif
}

auto Connection::RetrieveVersionInfoNoExceptions() const noexcept -> TransactionStatus {

  const GetVersionInfoRequest request;
  GetVersionInfoResponse response;
  grpc::ClientContext context;
  const grpc::Status grpc_status =
    this->general_info_service->GetVersionInfo(&context, request, &response);

  auto status = TransactionStatus::SUCCESS();
  if (!grpc_status.ok()) {
    switch (grpc_status.error_code()) {
    case grpc::StatusCode::UNAUTHENTICATED:
      status = TransactionStatus::AUTHENTICATION_ERROR(grpc_status.error_message());
      break;
    case grpc::StatusCode::UNAVAILABLE:
      status = TransactionStatus::CONNECTION_ERROR();
      break;
    default:
      auto error_message = grpc_status.error_message();
      status = TransactionStatus::RPC_ERROR(std::to_string(grpc_status.error_code()) + " - " +
                                            error_message);
    }
  } else {
    this->version_info = std::make_unique<VersionInfo>(response.release_version_info());
  }

  return status;
}

auto Connection::RetrieveVersionInfo() const -> const VersionInfo & {

  TransactionStatus status = RetrieveVersionInfoNoExceptions();
  status.ThrowExceptionIfError();
  return *GetVersionInfo();
}

[[nodiscard]] auto Connection::CreateTransaction() const -> std::unique_ptr<Transaction> {
  auto entity_service = this->entity_transaction_service;
  auto file_service = this->file_transmission_service;
  return std::make_unique<Transaction>(entity_service, file_service);
}

#ifdef BUILD_ACM
// TODO(tf) find a way to deal with this:
// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
[[nodiscard]] auto Connection::RetrieveSingleUser(const std::string &realm,
                                                  const std::string &name) const -> User {
  RetrieveSingleUserRequest request;
  request.set_name(name);
  request.set_realm(realm);
  RetrieveSingleUserResponse response;
  grpc::ClientContext context;
  const grpc::Status grpc_status =
    this->access_controll_management_service->RetrieveSingleUser(&context, request, &response);

  auto status = TransactionStatus::SUCCESS();
  if (!grpc_status.ok()) {
    switch (grpc_status.error_code()) {
    case grpc::StatusCode::UNAUTHENTICATED:
      status = TransactionStatus::AUTHENTICATION_ERROR(grpc_status.error_message());
      break;
    case grpc::StatusCode::UNAVAILABLE:
      status = TransactionStatus::CONNECTION_ERROR();
      break;
    default:
      auto error_message = grpc_status.error_message();
      status = TransactionStatus::RPC_ERROR(std::to_string(grpc_status.error_code()) + " - " +
                                            error_message);
    }
  }
  status.ThrowExceptionIfError();
  auto *user = response.release_user();
  return User(std::make_unique<UserImpl>(user));
}

// TODO(tf) find a way to deal with this:
// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
auto Connection::DeleteSingleUser(const std::string &realm, const std::string &name) const -> void {
  DeleteSingleUserRequest request;
  request.set_name(name);
  request.set_realm(realm);
  DeleteSingleUserResponse response;
  grpc::ClientContext context;
  const grpc::Status grpc_status =
    this->access_controll_management_service->DeleteSingleUser(&context, request, &response);

  auto status = TransactionStatus::SUCCESS();
  if (!grpc_status.ok()) {
    switch (grpc_status.error_code()) {
    case grpc::StatusCode::UNAUTHENTICATED:
      status = TransactionStatus::AUTHENTICATION_ERROR(grpc_status.error_message());
      break;
    case grpc::StatusCode::UNAVAILABLE:
      status = TransactionStatus::CONNECTION_ERROR();
      break;
    default:
      auto error_message = grpc_status.error_message();
      status = TransactionStatus::RPC_ERROR(std::to_string(grpc_status.error_code()) + " - " +
                                            error_message);
    }
  }
  status.ThrowExceptionIfError();
}

auto Connection::CreateSingleUser(const User &user) const -> void {
  CreateSingleUserRequest request;
  request.set_allocated_user(user.wrapped->wrapped);
  request.mutable_password_setting()->set_password(user.GetPassword());
  CreateSingleUserResponse response;
  grpc::ClientContext context;
  const grpc::Status grpc_status =
    this->access_controll_management_service->CreateSingleUser(&context, request, &response);

  auto status = TransactionStatus::SUCCESS();
  if (!grpc_status.ok()) {
    switch (grpc_status.error_code()) {
    case grpc::StatusCode::UNAUTHENTICATED:
      status = TransactionStatus::AUTHENTICATION_ERROR(grpc_status.error_message());
      break;
    case grpc::StatusCode::UNAVAILABLE:
      status = TransactionStatus::CONNECTION_ERROR();
      break;
    default:
      auto error_message = grpc_status.error_message();
      status = TransactionStatus::RPC_ERROR(std::to_string(grpc_status.error_code()) + " - " +
                                            error_message);
    }
  }
  status.ThrowExceptionIfError();
}

auto Connection::ListUsers() const -> std::vector<User> {
  ListUsersRequest request;
  ListUsersResponse response;
  grpc::ClientContext context;
  const grpc::Status grpc_status =
    this->access_controll_management_service->ListUsers(&context, request, &response);
  auto status = TransactionStatus::SUCCESS();
  if (!grpc_status.ok()) {
    switch (grpc_status.error_code()) {
    case grpc::StatusCode::UNAUTHENTICATED:
      status = TransactionStatus::AUTHENTICATION_ERROR(grpc_status.error_message());
      break;
    case grpc::StatusCode::UNAVAILABLE:
      status = TransactionStatus::CONNECTION_ERROR();
      break;
    default:
      auto error_message = grpc_status.error_message();
      status = TransactionStatus::RPC_ERROR(std::to_string(grpc_status.error_code()) + " - " +
                                            error_message);
    }
  }
  status.ThrowExceptionIfError();

  std::vector<User> results;
  for (auto &user : *(response.mutable_users())) {
    results.push_back(User(std::make_unique<UserImpl>(&user)));
  }

  return results;
}
#endif

auto ConnectionManager::mHasConnection(const std::string &name) const -> bool {
  auto it = connections.find(name);
  return it != connections.end();
}

auto ConnectionManager::mGetConnection(const std::string &name) const
  -> const std::shared_ptr<Connection> & {
  if (!HasConnection(name)) {
    try {
      auto connection = ConfigurationManager::GetConnectionConfiguration(name);
      connections[name] = std::make_shared<Connection>(*connection.release());
    } catch (const linkahead::exceptions::ConfigurationError &exc) {
      throw linkahead::exceptions::ConnectionConfigurationError(
        "Error with the connection named '" + name + "': " + exc.what());
    }
  }
  return this->connections.at(name);
}

auto ConnectionManager::mGetDefaultConnection() const -> const std::shared_ptr<Connection> & {
  if (!HasConnection(default_connection_name)) {
    default_connection_name = ConfigurationManager::GetDefaultConnectionName();
    auto default_connection = ConfigurationManager::GetDefaultConnectionConfiguration();
    connections[default_connection_name] =
      std::make_shared<Connection>(*default_connection.release());
  }

  return connections.at(default_connection_name);
}

} // namespace linkahead::connection
