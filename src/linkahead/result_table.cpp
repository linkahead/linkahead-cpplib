
/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/result_table.h"
#include <memory>                        // for unique_ptr
#include <string>                        // for string, operator==, basic_s...
#include <utility>                       // for move
#include <vector>                        // for vector
#include "caosdb/entity/v1/main.pb.h"    // for SelectQueryResult, SelectQu...
#include "linkahead/protobuf_helper.h"   // for ScalarProtoMessageWrapper
#include "linkahead/result_table_impl.h" // for ResultTableImpl, ResultTabl...
#include "linkahead/value.h"             // for Value
// IWYU pragma: no_include "net/proto2/public/repeated_field.h"

namespace linkahead::transaction {
using linkahead::entity::Value;
using ProtoSelectQueryResult = caosdb::entity::v1::SelectQueryResult;
using ProtoSelectQueryHeader = caosdb::entity::v1::SelectQueryHeader;
using ProtoSelectQueryColumn = caosdb::entity::v1::SelectQueryColumn;
using ProtoSelectQueryRow = caosdb::entity::v1::SelectQueryRow;
using linkahead::utility::ScalarProtoMessageWrapper;

ResultTableRowImpl::ResultTableRowImpl(ProtoSelectQueryResult *table, int row)
  : header(*table->mutable_header()), row(*table->mutable_data_rows(row)) {}

auto ResultTableRowImpl::GetColumnIndex(const std::string &column_name) const noexcept -> int {
  for (int i = 0; i < this->header.columns_size(); i++) {
    if (this->header.columns(i).name() == column_name) {
      return i;
    }
  }
  return -1;
}

auto ResultTableRowImpl::GetValue(const std::string &column) const noexcept -> Value {
  const auto column_index = GetColumnIndex(column);
  if (column_index == -1) {
    // NULL_VALUE
    return {};
  }
  Value result(this->row.cells(column_index));
  return result;
}

ResultTableRow::ResultTableRow(std::unique_ptr<ResultTableRowImpl> delegate)
  : delegate(std::move(delegate)) {}

auto ResultTableRow::GetValue(const std::string &column) const noexcept -> Value {
  return this->delegate->GetValue(column);
}

ResultTableColumnImpl::ResultTableColumnImpl(ProtoSelectQueryColumn *column)
  : ScalarProtoMessageWrapper<ProtoSelectQueryColumn>(column) {}

ResultTableColumn::ResultTableColumn(std::unique_ptr<ResultTableColumnImpl> delegate)
  : delegate(std::move(delegate)) {}

auto ResultTableColumn::GetName() const noexcept -> const std::string & {
  return this->delegate->wrapped->name();
}

auto ResultTableImpl::create(ProtoSelectQueryResult *select_result)
  -> std::unique_ptr<ResultTable> {
  return std::unique_ptr<ResultTable>(
    new ResultTable(std::unique_ptr<ResultTableImpl>(new ResultTableImpl(select_result))));
}

ResultTableImpl::ResultTableImpl(ProtoSelectQueryResult *result_table)
  : ScalarProtoMessageWrapper<ProtoSelectQueryResult>(result_table) {
  for (auto &column : *this->wrapped->mutable_header()->mutable_columns()) {
    this->columns.emplace_back(
      std::unique_ptr<ResultTableColumnImpl>(new ResultTableColumnImpl(&column)));
  }
  for (int i = 0; i < this->wrapped->data_rows_size(); i++) {
    this->rows.emplace_back(
      std::unique_ptr<ResultTableRowImpl>(new ResultTableRowImpl(this->wrapped, i)));
  }
}

ResultTable::ResultTable(std::unique_ptr<ResultTableImpl> delegate)
  : delegate(std::move(delegate)) {}

auto ResultTable::size() const noexcept -> int { return this->delegate->wrapped->data_rows_size(); }

auto ResultTable::GetRows() const noexcept -> RowIterator { return RowIterator(this, 0); }

ResultTable::RowIterator::RowIterator(const ResultTable *result_table_param, int index)
  : current_index(index), result_table(result_table_param) {}

ResultTable::RowIterator::RowIterator(const RowIterator &other) = default;

auto ResultTable::RowIterator::size() const noexcept -> int {
  return this->result_table->delegate->wrapped->data_rows_size();
}

auto ResultTable::RowIterator::operator*() const -> const ResultTableRow & {
  return this->result_table->delegate->rows.at(this->current_index);
}

auto ResultTable::RowIterator::operator++() -> RowIterator & {
  current_index++;
  return *this;
}

auto ResultTable::RowIterator::operator++(int) -> RowIterator {
  RowIterator tmp(*this);
  operator++();
  return tmp;
}

auto ResultTable::RowIterator::operator!=(const RowIterator &rhs) const -> bool {
  return this->current_index != rhs.current_index;
}

auto ResultTable::RowIterator::begin() const -> RowIterator {
  return RowIterator(this->result_table);
}

auto ResultTable::RowIterator::end() const -> RowIterator {
  return RowIterator(this->result_table, size());
}

auto ResultTable::GetHeader() const noexcept -> HeaderIterator { return HeaderIterator(this, 0); }

ResultTable::HeaderIterator::HeaderIterator(const ResultTable *result_table_param, int index)
  : current_index(index), result_table(result_table_param) {}

ResultTable::HeaderIterator::HeaderIterator(const HeaderIterator &other) = default;

auto ResultTable::HeaderIterator::size() const noexcept -> int {
  return this->result_table->delegate->wrapped->header().columns_size();
}

auto ResultTable::HeaderIterator::operator*() const -> const ResultTableColumn & {
  return this->result_table->delegate->columns.at(this->current_index);
}

auto ResultTable::HeaderIterator::operator++() -> HeaderIterator & {
  current_index++;
  return *this;
}

auto ResultTable::HeaderIterator::operator++(int) -> HeaderIterator {
  HeaderIterator tmp(*this);
  operator++();
  return tmp;
}

auto ResultTable::HeaderIterator::operator!=(const HeaderIterator &rhs) const -> bool {
  return this->current_index != rhs.current_index;
}

auto ResultTable::HeaderIterator::begin() const -> HeaderIterator {
  return HeaderIterator(this->result_table);
}

auto ResultTable::HeaderIterator::end() const -> HeaderIterator {
  return HeaderIterator(this->result_table, size());
}

} // namespace linkahead::transaction
