/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/utility.h"
#include <boost/beast/core/detail/base64.hpp> // for encoded_size
#include <boost/beast/core/detail/base64.ipp> // for encode
#include <boost/json/stream_parser.hpp>       // for stream_parser
#include <boost/json/value.hpp>               // for value
#include <cassert>                            // for assert
#include <cstdint>                            // for uintmax_t
#include <limits>                             // for numeric_limits
#include <map>                                // for map, _Rb_tree_const_it...
#include <memory>                             // for shared_ptr, allocator
#include <stdexcept>                          // for out_of_range, length_e...
#include <type_traits>                        // for underlying_type_t
#include <typeinfo>                           // for type_info
#include <utility>                            // for pair, move
#include "linkahead/data_type.h"              // for AtomicDataType, atomic...
#include "linkahead/entity.h"                 // for Importance, Role, impo...

namespace linkahead::utility {

using boost::json::stream_parser;
using boost::json::value;
using linkahead::entity::AtomicDataType;
using linkahead::entity::Importance;
using linkahead::entity::Role;

template <typename Enum> auto getEnumNameFromValue(Enum v) -> std::string {
  if (std::is_same_v<std::underlying_type_t<Enum>, int>) {
    return std::to_string(static_cast<int>(v));
  }
  throw std::logic_error(std::string("Enum type ") + typeid(v).name() + " not implemented.");
}

// Enum helper template specializations //////////////////////////////////////
template <> auto getEnumNameFromValue<Importance>(Importance v) -> std::string {
  auto result = linkahead::entity::importance_names.at(v);
  return result;
}

template <> auto getEnumNameFromValue<Role>(Role v) -> std::string {
  auto result = linkahead::entity::role_names.at(v);
  return result;
}

template <> auto getEnumNameFromValue<AtomicDataType>(AtomicDataType v) -> std::string {
  auto result = linkahead::entity::atomicdatatype_names.at(v);
  return result;
}

template <> auto getEnumValueFromName<Importance>(const std::string &name) -> Importance {
  // TODO (dh): Why does this compile?
  // if (linkahead::entity::importance_names.begin()->second == name) {}
  // std::for_each(linkahead::entity::importance_names.begin(),
  //               linkahead::entity::importance_names.end(),
  //               [](const auto &entry){});
  // TODO (dh): Whereas this does not?
  // auto result = std::find(linkahead::entity::importance_names.cbegin(),
  //                         linkahead::entity::importance_names.cend(),
  //                         [name](const auto& entry){ return entry.second == name; });
  // Workaround: plaint old iteration:
  for (auto const &entry : linkahead::entity::importance_names) {
    if (entry.second == name) {
      return entry.first;
    }
  }
  throw std::out_of_range(std::string("Could not find enum value for string '") + name + "'.");
}

template <> auto getEnumValueFromName<AtomicDataType>(const std::string &name) -> AtomicDataType {
  for (auto const &entry : linkahead::entity::atomicdatatype_names) {
    if (entry.second == name) {
      return entry.first;
    }
  }
  throw std::out_of_range(std::string("Could not find enum value for string '") + name + "'.");
}

template <> auto getEnumValueFromName<Role>(const std::string &name) -> Role {
  for (auto const &entry : linkahead::entity::role_names) {
    if (entry.second == name) {
      return entry.first;
    }
  }
  throw std::out_of_range(std::string("Could not find enum value for string '") + name + "'.");
}

// End of template specialization /////////////////////////////////////////////

auto load_string_file(const path &file_path) -> std::string {
  std::string result;

  if (!exists(file_path)) {
    throw std::runtime_error("File not found: " + file_path.string());
  }

  // adapted from boost::filesystem::load_string_file, which was removed in 1.84
  std::ifstream file;
  file.exceptions(std::ios_base::failbit | std::ios_base::badbit);
  file.open(file_path, std::ios_base::binary);
  const std::uintmax_t sz = std::filesystem::file_size(file_path);
  if ((sz > static_cast<std::uintmax_t>((std::numeric_limits<std::streamsize>::max)()))) {
    throw(std::length_error("File size exceeds max read size"));
  }
  result.resize(static_cast<std::size_t>(sz), '\0');
  if (sz > 0U) {
    file.read(result.data(), static_cast<std::streamsize>(sz));
  }

  return result;
}

auto base64_encode(const std::string &plain) -> std::string {
  auto size_plain = plain.size();
  auto size_encoded = boost::beast::detail::base64::encoded_size(size_plain);

  std::string result = std::string(size_encoded, '\0');
  boost::beast::detail::base64::encode(result.data(), plain.c_str(), size_plain);

  return result;
}

auto _load_json_file(const path &json_file) -> value {
  assert(exists(json_file));

  constexpr auto buffer_size = static_cast<size_t>(4096);
  auto stream = ifstream(json_file);
  stream.exceptions(std::ios_base::badbit);

  stream_parser parser;
  auto buffer = std::string(buffer_size, '\0');
  while (stream.read(buffer.data(), buffer_size)) {
    parser.write(buffer.c_str(), stream.gcount());
  }
  parser.write(buffer.c_str(), stream.gcount());

  assert(parser.done());
  return parser.release();
}

auto load_json_file(const path &json_file) -> JsonValue {
  return {new value(_load_json_file(json_file))};
}

JsonValue::JsonValue(void *wrapped) { this->wrapped.reset(static_cast<value *>(wrapped)); }

JsonValue::~JsonValue() = default;

auto JsonValue::Reset() -> void {
  if (this->wrapped) {
    this->wrapped.reset();
  }
}

JsonValue::JsonValue(const JsonValue &other) {
  if (other.wrapped) {
    this->wrapped.reset(static_cast<value *>(other.wrapped.get()));
  }
}

auto JsonValue::operator=(const JsonValue &other) -> JsonValue & {
  if (this != &other) {
    if (other.wrapped) {
      this->wrapped = std::make_shared<value>(*(static_cast<value *>(other.wrapped.get())));
    }
  }
  return *this;
}

JsonValue::JsonValue(JsonValue &&other) noexcept {
  if (other.wrapped) {
    this->wrapped = std::move(other.wrapped);
  }
}

auto JsonValue::operator=(JsonValue &&other) noexcept -> JsonValue & {
  if (this != &other) {
    Reset();
    this->wrapped = std::move(other.wrapped);
  }
  return *this;
}

} // namespace linkahead::utility
