/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/entity.h"
#include "linkahead/data_type.h"       // for DataType
#include "caosdb/entity/v1/main.pb.h"  // for Messages
#include "linkahead/protobuf_helper.h" // for get_arena
#include "linkahead/status_code.h"     // for StatusCode
#include <google/protobuf/arena.h>     // for Arena
#include <string>                      // for string
#include <vector>                      // for vector

namespace linkahead::entity {
using ProtoParent = caosdb::entity::v1::Parent;
using ProtoProperty = caosdb::entity::v1::Property;
using ProtoEntity = caosdb::entity::v1::Entity;
using ProtoImportance = caosdb::entity::v1::Importance;
using caosdb::entity::v1::EntityRole;
using ProtoMessage = caosdb::entity::v1::Message;
using ProtoFileDescriptor = caosdb::entity::v1::FileDescriptor;
using google::protobuf::Arena;
using linkahead::utility::get_arena;

// Parent /////////////////////////////////////////////////////////////////////

auto Parent::SetName(const std::string &name) -> void { this->wrapped->set_name(name); }

auto Parent::SetId(const std::string &id) -> void { this->wrapped->set_id(id); }

[[nodiscard]] auto Parent::GetId() const -> const std::string & { return this->wrapped->id(); }

[[nodiscard]] auto Parent::GetName() const -> const std::string & { return this->wrapped->name(); }

[[nodiscard]] auto Parent::GetDescription() const -> const std::string & {
  return this->wrapped->description();
}

// Property ///////////////////////////////////////////////////////////////////

[[nodiscard]] auto Property::GetId() const -> const std::string & { return this->wrapped->id(); }

[[nodiscard]] auto Property::GetName() const -> const std::string & {
  return this->wrapped->name();
}

[[nodiscard]] auto Property::GetDescription() const -> const std::string & {
  return this->wrapped->description();
}

[[nodiscard]] auto Property::GetImportance() const -> Importance {
  return static_cast<Importance>(this->wrapped->importance());
}

[[nodiscard]] auto Property::GetValue() const -> const Value & { return this->value; }

[[nodiscard]] auto Property::GetUnit() const -> const std::string & {
  return this->wrapped->unit();
}

[[nodiscard]] auto Property::GetDataType() const -> const DataType & { return this->data_type; }

auto Property::SetId(const std::string &id) -> void { this->wrapped->set_id(id); }

auto Property::SetName(const std::string &name) -> void { this->wrapped->set_name(name); }

auto Property::SetDescription(const std::string &description) -> void {
  this->wrapped->set_description(description);
}

auto Property::SetImportance(Importance importance) -> void {
  this->wrapped->set_importance(static_cast<ProtoImportance>(importance));
}

auto Property::SetValue(const Value &value) -> StatusCode {
  if (value.wrapped == nullptr) {
    this->wrapped->clear_value();
    this->value = Value();
  } else {
    this->wrapped->mutable_value()->CopyFrom(*value.wrapped);
    this->value = Value(this->wrapped->mutable_value());
  }
  return StatusCode::SUCCESS;
}

auto Property::SetValue(const AbstractValue &value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(const std::string &value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(const char *value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(double value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(const std::vector<std::string> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const std::vector<char *> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const std::vector<int64_t> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const std::vector<int> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const std::vector<double> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const std::vector<bool> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Property::SetValue(const int64_t value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(const int value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetValue(const bool value) -> StatusCode { return SetValue(Value(value)); }

auto Property::SetUnit(const std::string &unit) -> void { this->wrapped->set_unit(unit); }

auto Property::SetDataType(const DataType &new_data_type) -> StatusCode {
  if (new_data_type.wrapped == nullptr) {
    this->wrapped->clear_data_type();
    this->data_type = DataType();
  } else {
    this->wrapped->mutable_data_type()->CopyFrom(*new_data_type.wrapped);
    this->data_type = DataType(this->wrapped->mutable_data_type());
  }
  return StatusCode::SUCCESS;
}

auto Property::SetDataType(const AtomicDataType new_data_type, bool list_type) -> StatusCode {
  return SetDataType(DataType(new_data_type, list_type));
}

auto Property::SetDataType(const std::string &new_data_type, bool list_type) -> StatusCode {
  return SetDataType(DataType(new_data_type, list_type));
}

// Entity /////////////////////////////////////////////////////////////////////
[[nodiscard]] auto Entity::GetParents() const -> const Parents & { return parents; }

auto Entity::AppendParent(const Parent &parent) -> void { this->parents.Append(parent); }

auto Entity::RemoveParent(int index) -> void { this->parents.Remove(index); }

[[nodiscard]] auto Entity::GetProperties() const -> const Properties & { return properties; }

auto Entity::AppendProperty(const Property &property) -> void { this->properties.Append(property); }

auto Entity::RemoveProperty(int index) -> void { this->properties.Remove(index); }

auto Entity::CreateMessagesField() -> RepeatedPtrField<ProtoMessage> * {
  return Arena::CreateMessage<RepeatedPtrField<ProtoMessage>>(get_arena());
}

auto Entity::SetId(const std::string &id) -> void { this->wrapped->set_id(id); }

auto Entity::SetVersionId(const std::string &id) -> void {
  this->wrapped->mutable_version()->set_id(id);
}

auto Entity::CopyTo(ProtoEntity *target) -> void { target->CopyFrom(*(this->wrapped)); }

auto Entity::SetRole(Role role) -> void { this->wrapped->set_role(static_cast<EntityRole>(role)); }

auto Entity::SetName(const std::string &name) -> void { this->wrapped->set_name(name); }

auto Entity::SetDescription(const std::string &description) -> void {
  this->wrapped->set_description(description);
}

auto Entity::SetValue(const Value &value) -> StatusCode {
  if (GetRole() != Role::PROPERTY) {
    return StatusCode::ENTITY_CANNOT_HAVE_A_VALUE;
  }
  if (value.wrapped == nullptr) {
    this->wrapped->clear_value();
    this->value = Value();
  } else {
    this->wrapped->mutable_value()->CopyFrom(*value.wrapped);
    this->value = Value(this->wrapped->mutable_value());
  }
  return StatusCode::SUCCESS;
}

auto Entity::SetValue(const AbstractValue &value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(const std::string &value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(const char *value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(double value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(const std::vector<std::string> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const std::vector<char *> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const std::vector<int64_t> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const std::vector<int> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const std::vector<double> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const std::vector<bool> &values) -> StatusCode {
  return SetValue(Value(values));
}

auto Entity::SetValue(const int64_t value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(const int value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetValue(const bool value) -> StatusCode { return SetValue(Value(value)); }

auto Entity::SetUnit(const std::string &unit) -> void { this->wrapped->set_unit(unit); }

auto Entity::SetDataType(const DataType &new_data_type) -> StatusCode {
  if (GetRole() != Role::PROPERTY) {
    return StatusCode::ENTITY_CANNOT_HAVE_A_DATA_TYPE;
  }
  if (new_data_type.wrapped == nullptr) {
    this->wrapped->clear_data_type();
    this->data_type = DataType();
  } else {
    this->wrapped->mutable_data_type()->CopyFrom(*new_data_type.wrapped);
    this->data_type = DataType(this->wrapped->mutable_data_type());
  }
  return StatusCode::SUCCESS;
}

auto Entity::SetDataType(const AtomicDataType new_data_type, bool list_type) -> StatusCode {
  return SetDataType(DataType(new_data_type, list_type));
}

auto Entity::SetDataType(const std::string &new_data_type, bool list_type) -> StatusCode {
  return SetDataType(DataType(new_data_type, list_type));
}

auto Entity::SetFilePath(const std::string &path) -> void {
  this->wrapped->mutable_file_descriptor()->set_path(path);
}

} // namespace linkahead::entity
