/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/logging.h"
#include <boost/core/swap.hpp> // for swap
#include <boost/iterator/iterator_facade.hpp>
#include <boost/log/attributes/clock.hpp>
#include <boost/log/core/core.hpp> // for core
#include <boost/log/core/record.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/utility/setup/from_settings.hpp>
#include <boost/log/utility/setup/settings.hpp>
#include <boost/multi_index/detail/bidir_node_iterator.hpp>
#include <boost/operators.hpp>
#include <boost/preprocessor/seq/limits/enum_256.hpp>
#include <boost/preprocessor/seq/limits/size_256.hpp>
#include <boost/property_tree/detail/exception_implementation.hpp>
#include <boost/property_tree/detail/ptree_implementation.hpp>
#include <boost/smart_ptr/intrusive_ref_counter.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <cstdint>
#include <filesystem> // for path
#include <memory>
#include <string>
#include <utility> // for move
#include <vector>
#include "linkahead/log_level.h"

namespace linkahead::logging {
using boost_logger_class = boost::log::sources::severity_channel_logger_mt<int, std::string>;

class logger {
public:
  static auto get() -> boost_logger_class & { return logger::GetInstance()._logger_instance; }

private:
  static logger &GetInstance() {
    static logger instance;
    return instance;
  }
  boost_logger_class _logger_instance;
};

LoggerOutputStream::LoggerOutputStream(std::string channel, int level)
  : channel(std::move(channel)), level(level) {}

auto LoggerOutputStream::operator<<(std::ostream &(*f)(std::ostream &)) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), this->channel, this->level) << f;

  return *this;
}

auto LoggerOutputStream::operator<<(bool msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), this->channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(std::streambuf *msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(int msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(int64_t msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(uint64_t msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(const char *msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(const std::string &msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(void *msg) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << msg;

  return *this;
}

auto LoggerOutputStream::operator<<(std::filesystem::path *path) -> LoggerOutputStream & {
  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << path->string();

  return *this;
}

LoggerOutputStream &LoggerOutputStream::operator<<(const std::filesystem::path &path) {

  BOOST_LOG_CHANNEL_SEV(linkahead::logging::logger::get(), channel, this->level) << path.string();
  return *this;
}

LoggingConfiguration::LoggingConfiguration(int level) : LevelConfiguration(level) {}

auto LoggingConfiguration::AddSink(const std::shared_ptr<SinkConfiguration> &sink) -> void {
  this->sinks.push_back(sink);
}

auto LoggingConfiguration::GetSinks() const
  -> const std::vector<std::shared_ptr<SinkConfiguration>> & {
  return this->sinks;
}

SinkConfiguration::SinkConfiguration(std::string name, int level)
  : LevelConfiguration(level), name(std::move(name)) {}

[[nodiscard]] auto SinkConfiguration::GetName() const -> const std::string & { return this->name; }

auto SinkConfiguration::Configure(void *settings) const -> void {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter SinkConfiguration::Configure(*settings)";
  auto sink = "Sinks." + GetName();
  auto *boost_settings = static_cast<boost::log::settings *>(settings);
  (*boost_settings)[sink]["Destination"] = GetDestination();
  (*boost_settings)[sink]["Filter"] = "%Severity% >= " + std::to_string(GetLevel());
  (*boost_settings)[sink]["AutoFlush"] = true;
  (*boost_settings)[sink]["Format"] = "[%TimeStamp%][%Severity%] %Channel% - %Message%";
}

ConsoleSinkConfiguration::ConsoleSinkConfiguration(const std::string &name, int level)
  : SinkConfiguration(name, level) {}

[[nodiscard]] auto ConsoleSinkConfiguration::GetDestination() const -> const std::string & {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter ConsoleSinkConfiguration::GetDestination()";
  return this->destination;
}

auto ConsoleSinkConfiguration::Configure(void *settings) const -> void {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter ConsoleSinkConfiguration::Configure(*settings)";
  sink_configuration::Configure(settings);
}

FileSinkConfiguration::FileSinkConfiguration(const std::string &name, int level)
  : SinkConfiguration(name, level) {}

[[nodiscard]] auto FileSinkConfiguration::GetDestination() const -> const std::string & {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter FileSinkConfiguration::GetDestination()";
  return this->destination;
}

auto FileSinkConfiguration::SetDirectory(const std::string &directory) -> void {
  this->directory = std::string(directory);
}

auto FileSinkConfiguration::Configure(void *settings) const -> void {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter FileSinkConfiguration::Configure(*settings)";
  sink_configuration::Configure(settings);
  auto *boost_settings = static_cast<boost::log::settings *>(settings);
  (*boost_settings)["Sink." + GetName() + ".Target"] = this->directory;
}

SyslogSinkConfiguration::SyslogSinkConfiguration(const std::string &name, int level)
  : SinkConfiguration(name, level) {}

[[nodiscard]] auto SyslogSinkConfiguration::GetDestination() const -> const std::string & {
  return this->destination;
}

// Called if no custom logging settings are specified.
auto initialize_logging_defaults() -> int {
  // first: turn everything off
  auto core = boost::log::core::get();
  if (core->get_logging_enabled()) {
    core->flush();
    core->remove_all_sinks();
    core->set_logging_enabled(false);
  }

  // now set everything up
  const static std::vector<std::shared_ptr<SinkConfiguration>> default_sinks = {
    std::make_shared<ConsoleSinkConfiguration>("DEFAULT_SINK_1", LINKAHEAD_DEFAULT_LOG_LEVEL)};

  boost::log::settings default_settings;

  default_settings["Core.DisableLogging"] = false;

  for (const auto &sink : default_sinks) {
    sink->Configure(&default_settings);
  }

  boost::log::init_from_settings(default_settings);
  core->add_global_attribute("TimeStamp", boost::log::attributes::local_clock());

  LINKAHEAD_LOG_DEBUG(logger_name) << "Initialized default settings.";

  return 0;
}

// Called if custom logging settings are specified.
auto initialize_logging(const LoggingConfiguration &configuration) -> void {
  // first: turn everything off
  auto core = boost::log::core::get();
  if (core->get_logging_enabled()) {
    core->flush();
    core->remove_all_sinks();
    core->set_logging_enabled(false);
  }

  if (configuration.GetLevel() == LINKAHEAD_LOG_LEVEL_OFF) {
    // it is off
    return;
  }

  // now set everything up
  boost::log::settings new_settings;
  new_settings["Core.DisableLogging"] = false;

  for (const auto &sink : configuration.GetSinks()) {
    sink->Configure(&new_settings);
  }

  boost::log::init_from_settings(new_settings);

  LINKAHEAD_LOG_DEBUG(logger_name) << "Initialized logging with custom settings.";
}

void linkahead_log_fatal(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_FATAL) << msg;
}

void linkahead_log_error(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_ERROR) << msg;
}

void linkahead_log_warn(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_WARN) << msg;
}

void linkahead_log_info(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_INFO) << msg;
}

void linkahead_log_debug(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_DEBUG) << msg;
}

void linkahead_log_trace(const char *channel, const char *msg) {
  LoggerOutputStream::get(channel, LINKAHEAD_LOG_LEVEL_TRACE) << msg;
}

} // namespace linkahead::logging
