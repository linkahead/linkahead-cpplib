/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#include "linkahead/file_transmission/register_file_upload_handler.h"
#include "linkahead/logging.h" // for LINKAHEAD_LOG_TRACE

namespace linkahead::transaction {

RegisterFileUploadHandler::~RegisterFileUploadHandler() = default;

RegisterFileUploadHandler::RegisterFileUploadHandler(HandlerTag tag,
                                                     FileTransmissionService::Stub *stub,
                                                     grpc::CompletionQueue *completion_queue,
                                                     RegisterFileUploadRequest *request,
                                                     RegisterFileUploadResponse *response)
  : UnaryRpcHandler(completion_queue), tag_(tag), stub_(stub), request_(request),
    response_(response) {}

void RegisterFileUploadHandler::handleNewCallState() {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter RegisterFileUploadHandler::handleNewCallState.";

  rpc_ = stub_->PrepareAsyncRegisterFileUpload(&call_context, *request_, completion_queue);

  state_ = CallState::CallComplete;
  rpc_->StartCall();
  rpc_->Finish(response_, &status_, tag_);

  LINKAHEAD_LOG_TRACE(logger_name) << "Leave RegisterFileUploadHandler::handleNewCallState";
}

} // namespace linkahead::transaction
