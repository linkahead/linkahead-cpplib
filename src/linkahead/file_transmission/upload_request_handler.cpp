/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#include "linkahead/file_transmission/upload_request_handler.h"
#include "linkahead/logging.h"               // for LINKAHEAD_LOG_ERROR
#include "linkahead/protobuf_helper.h"       // for get_arena
#include "linkahead/status_code.h"           // for GENERIC_RPC_E...
#include "linkahead/transaction_status.h"    // for TransactionStatus
#include <algorithm>                         // for min
#include <cstdint>                           // for uint64_t
#include <exception>                         // IWYU pragma: keep
#include <filesystem>                        // for operator<<, path
#include <google/protobuf/arena.h>           // for Arena
#include <grpcpp/impl/codegen/call_op_set.h> // for WriteOptions
#include <string>                            // for basic_string
#include <utility>                           // for move
// IWYU pragma: no_include <bits/exception.h>

namespace linkahead::transaction {
using google::protobuf::Arena;
using linkahead::StatusCode;
using linkahead::utility::get_arena;

UploadRequestHandler::UploadRequestHandler(HandlerTag tag, FileTransmissionService::Stub *stub,
                                           grpc::CompletionQueue *cq,
                                           FileDescriptor file_descriptor)
  : tag_(tag), stub_(stub), cq_(cq), request_(Arena::CreateMessage<FileUploadRequest>(get_arena())),
    response_(Arena::CreateMessage<FileUploadResponse>(get_arena())), state_(CallState::NewCall),
    file_descriptor_(std::move(file_descriptor)), bytesToSend_(0) {}

bool UploadRequestHandler::OnNext(bool ok) {
  try {
    if (state_ == CallState::CallComplete) {
      this->handleCallCompleteState();
      return false;
    } else if (ok) {
      if (state_ == CallState::NewCall) {
        this->handleNewCallState();
      } else if (state_ == CallState::SendingHeader) {
        this->handleSendingHeaderState();
      } else if (state_ == CallState::SendingFile) {
        this->handleSendingFileState();
      } else if (state_ == CallState::ExpectingResponse) {
        this->handleExpectingResponseState();
      }
    } else {
      state_ = CallState::CallComplete;
      rpc_->Finish(&status_, tag_);
    }

    return true;
  } catch (std::exception &e) {
    LINKAHEAD_LOG_ERROR(logger_name) << "UploadRequestHandler caught an exception: " << e.what();
    transaction_status = TransactionStatus::GENERIC_ERROR(e.what());
    state_ = CallState::CallComplete;
  } catch (...) {
    LINKAHEAD_LOG_ERROR(logger_name) << "Transaction error: unknown exception caught";
    transaction_status =
      TransactionStatus::GENERIC_ERROR("UploadRequestHandler caught an unknown exception");
    state_ = CallState::CallComplete;
  }

  if (state_ == CallState::NewCall) {
    return false;
  }

  ctx_.TryCancel();

  return true;
}

void UploadRequestHandler::Cancel() {
  state_ = CallState::CallComplete;
  ctx_.TryCancel();
}

void UploadRequestHandler::handleNewCallState() {
  auto filename = file_descriptor_.local_path;
  fileReader_ = std::make_unique<FileReader>(filename);

  rpc_ = stub_->PrepareAsyncFileUpload(&ctx_, response_, cq_);

  transaction_status = TransactionStatus::EXECUTING();
  state_ = CallState::SendingHeader;
  rpc_->StartCall(tag_);
}

void UploadRequestHandler::handleSendingHeaderState() {
  auto *tid = request_->mutable_chunk()->mutable_file_transmission_id();
  tid->CopyFrom(*(file_descriptor_.file_transmission_id));

  bytesToSend_ = fileReader_->fileSize();

  if (bytesToSend_ > 0) {
    state_ = CallState::SendingFile;
  } else {
    state_ = CallState::ExpectingResponse;
  }

  rpc_->Write(*request_, tag_);
}

void UploadRequestHandler::handleSendingFileState() {
  const auto DefaultChunkSize = static_cast<uint64_t>(4 * 1024); // 4K

  auto chunkSize = std::min(DefaultChunkSize, bytesToSend_);

  request_->Clear();
  auto *buffer = request_->mutable_chunk()->mutable_data();
  buffer->resize(chunkSize);

  fileReader_->read(*buffer);
  bytesToSend_ -= chunkSize;

  grpc::WriteOptions writeOptions;
  if (bytesToSend_ > 0) {
    state_ = CallState::SendingFile;
  } else {
    state_ = CallState::ExpectingResponse;
    writeOptions.set_last_message();
  }

  rpc_->Write(*request_, writeOptions, tag_);
}

void UploadRequestHandler::handleExpectingResponseState() {
  state_ = CallState::CallComplete;
  rpc_->Finish(&status_, tag_);
}

void UploadRequestHandler::handleCallCompleteState() {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter UploadRequestHandler::handleCallCompleteState";

  switch (status_.error_code()) {
  case grpc::OK: {
    auto bytesSent = fileReader_ != nullptr ? fileReader_->fileSize() : 0;
    LINKAHEAD_LOG_INFO(logger_name)
      << "UploadRequestHandler finished successfully (" << file_descriptor_.local_path
      << "): upload complete, " << bytesSent << " bytes sent";
  } break;
  default: {
    auto code(static_cast<StatusCode>(status_.error_code()));
    std::string description(get_status_description(code) +
                            " Original message: " + status_.error_message());
    transaction_status = TransactionStatus(code, description);
    LINKAHEAD_LOG_ERROR(logger_name)
      << "UploadRequestHandler finished with an error (" << file_descriptor_.local_path
      << "): Upload aborted with code " << code << " - " << description;
  } break;
  }

  LINKAHEAD_LOG_TRACE(logger_name) << "Leave UploadRequestHandler::handleCallCompleteState";
}

} // namespace linkahead::transaction
