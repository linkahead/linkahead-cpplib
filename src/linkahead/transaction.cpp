/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/transaction.h"
#include <google/protobuf/arena.h>
#include <grpc/grpc.h>
#include <filesystem>
#include <future>
#include <map>
#include <memory>
#include <random>
#include <string>
#include <utility>
#include "caosdb/entity/v1/main.grpc.pb.h"
#include "caosdb/entity/v1/main.pb.h"
#include "linkahead/file_transmission/download_request_handler.h"
#include "linkahead/file_transmission/register_file_upload_handler.h"
#include "linkahead/file_transmission/upload_request_handler.h"
#include "linkahead/logging.h"
#include "linkahead/protobuf_helper.h"
#include "linkahead/result_set.h"
#include "linkahead/result_table.h"
#include "linkahead/result_table_impl.h"
#include "linkahead/status_code.h"
#include "linkahead/transaction_handler.h"
// IWYU pragma: no_include <cxxabi.h>
// IWYU pragma: no_include "net/proto2/public/repeated_field.h"

namespace linkahead::transaction {
using caosdb::entity::v1::EntityTransactionService;
using caosdb::entity::v1::FileTransmissionService;
using caosdb::entity::v1::MultiTransactionRequest;
using caosdb::entity::v1::MultiTransactionResponse;
using TransactionResponseCase = caosdb::entity::v1::TransactionResponse::TransactionResponseCase;
using RetrieveResponseCase = caosdb::entity::v1::RetrieveResponse::RetrieveResponseCase;
using RetrieveResponse = caosdb::entity::v1::RetrieveResponse;
using ProtoEntity = caosdb::entity::v1::Entity;
using ProtoSelectQueryResult = caosdb::entity::v1::SelectQueryResult;
using caosdb::entity::v1::EntityRequest;

using google::protobuf::Arena;
using NextStatus = grpc::CompletionQueue::NextStatus;
using RegistrationStatus = caosdb::entity::v1::RegistrationStatus;

Transaction::Transaction(std::shared_ptr<EntityTransactionService::Stub> entity_service,
                         std::shared_ptr<FileTransmissionService::Stub> file_service)
  : entity_service(std::move(entity_service)), file_service(std::move(file_service)),
    request(Arena::CreateMessage<MultiTransactionRequest>(GetArena())),
    response(Arena::CreateMessage<MultiTransactionResponse>(GetArena())), query_count(-1),
    result_set(std::make_unique<MultiResultSet>(std::vector<std::unique_ptr<Entity>>())) {}

auto Transaction::RetrieveById(const std::string &id) noexcept -> StatusCode {
  ASSERT_CAN_ADD_RETRIEVAL

  auto *sub_request = this->request->add_requests();
  sub_request->mutable_retrieve_request()->set_id(id);

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto Transaction::RetrieveAndDownloadFileById(const std::string &id,
                                              const std::string &local_path) noexcept
  -> StatusCode {
  ASSERT_CAN_ADD_RETRIEVAL

  auto *retrieve_request = this->request->add_requests()->mutable_retrieve_request();
  retrieve_request->set_id(id);
  retrieve_request->set_register_file_download(true);

  download_files[id].local_path = local_path;

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto Transaction::Query(const std::string &query) noexcept -> StatusCode {
  ASSERT_CAN_ADD_QUERY

  // currently, the server can only process one query at a time (but mixed with
  // other retrievals).
  this->has_query = true;
  auto *sub_request = this->request->add_requests();
  sub_request->mutable_retrieve_request()->mutable_query()->set_query(query);

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto Transaction::DeleteById(const std::string &id) noexcept -> StatusCode {
  ASSERT_CAN_ADD_DELETION

  auto *sub_request = this->request->add_requests();
  sub_request->mutable_delete_request()->set_id(id);

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto get_next_file_id() -> std::string {
  const std::string str = "0123456789abcdef";
  std::mt19937 generator(std::random_device{}());
  std::uniform_int_distribution<int> distribution(0, 15); // NOLINT
  std::string result(10, '\0');                           // NOLINT

  for (auto &dis : result) {
    dis = str[distribution(generator)];
  }
  return result;
}

// only used in the next two functions.
auto add_entity_to_request(Entity *entity, EntityRequest *entity_request,
                           std::vector<FileDescriptor> *upload_files) -> void {
  auto *proto_entity = entity_request->mutable_entity();
  entity->CopyTo(proto_entity);
  if (entity->HasFile()) {
    auto *file_transmission_id = entity_request->mutable_upload_id();
    file_transmission_id->set_file_id(get_next_file_id());
    entity->SetFileTransmissionId(file_transmission_id);
    upload_files->push_back(entity->GetFileDescriptor());
  }
}

auto Transaction::InsertEntity(Entity *entity) noexcept -> StatusCode {
  ASSERT_CAN_ADD_INSERTION

  auto *entity_request =
    this->request->add_requests()->mutable_insert_request()->mutable_entity_request();

  add_entity_to_request(entity, entity_request, &upload_files);

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto Transaction::UpdateEntity(Entity *entity) noexcept -> StatusCode {
  ASSERT_CAN_ADD_UPDATE

  auto *entity_request =
    this->request->add_requests()->mutable_update_request()->mutable_entity_request();

  add_entity_to_request(entity, entity_request, &upload_files);

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

auto Transaction::Execute() -> TransactionStatus {
  auto status_code = ExecuteAsynchronously();
  TransactionStatus::ThrowExceptionIfError(status_code,
                                           linkahead::get_status_description(status_code));
  auto status = WaitForIt();
  status.ThrowExceptionIfError();
  return status;
}

// NOLINTNEXTLINE
#define TRANSACTION_SYNCRONIZED_BLOCK                                                              \
  const std::lock_guard<std::mutex> lock(this->transaction_mutex);

// NOLINTNEXTLINE
auto Transaction::DoExecuteTransaction() noexcept -> void {
  // upload files first
  if (!upload_files.empty()) {
    LINKAHEAD_LOG_INFO(logger_name) << "Number of files to be uploaded: " << upload_files.size();

    auto *registration_request = Arena::CreateMessage<RegisterFileUploadRequest>(get_arena());
    auto *registration_response = Arena::CreateMessage<RegisterFileUploadResponse>(get_arena());

    {
      TRANSACTION_SYNCRONIZED_BLOCK
      if (this->status.GetCode() == StatusCode::EXECUTING) {
        handler_ = std::make_unique<RegisterFileUploadHandler>(
          &handler_, file_service.get(), &completion_queue, registration_request,
          registration_response);
      }
    }
    this->status = ProcessCalls();

    if (registration_response->status() != RegistrationStatus::REGISTRATION_STATUS_ACCEPTED) {
      this->status = TransactionStatus::FILE_UPLOAD_ERROR();
    }

    for (auto &file_descriptor : upload_files) {
      file_descriptor.file_transmission_id->set_registration_id(
        registration_response->registration_id());
      {
        TRANSACTION_SYNCRONIZED_BLOCK
        if (this->status.GetCode() == StatusCode::EXECUTING) {
          LINKAHEAD_LOG_INFO(logger_name) << "Uploading " << file_descriptor.local_path;
          handler_ = std::make_unique<UploadRequestHandler>(&handler_, file_service.get(),
                                                            &completion_queue, file_descriptor);
        }
      }
      this->status = ProcessCalls();
      if (this->status.GetCode() != StatusCode::EXECUTING) {
        // Return early, there has been an error.
        return;
      }
    }
  }

  if (this->status.GetCode() == StatusCode::EXECUTING) {
    {
      TRANSACTION_SYNCRONIZED_BLOCK
      if (this->status.GetCode() == StatusCode::EXECUTING) {
        LINKAHEAD_LOG_DEBUG(logger_name) << "RPC Request: " << RequestToString();
        handler_ = std::make_unique<EntityTransactionHandler>(&handler_, entity_service.get(),
                                                              &completion_queue, request, response);
      }
    }
    this->status = ProcessCalls();
  }

  // file download afterwards
  if (status.GetCode() == StatusCode::EXECUTING && !download_files.empty()) {
    // run over all retrieved entities and get the download_id
    for (auto sub_response : *(response->mutable_responses())) {
      if (sub_response.transaction_response_case() == TransactionResponseCase::kRetrieveResponse) {
        if (sub_response.retrieve_response().entity_response().has_download_id()) {
          auto *entity_response =
            sub_response.mutable_retrieve_response()->mutable_entity_response();
          auto entity_id = entity_response->entity().id();
          download_files[entity_id].file_transmission_id =
            Arena::CreateMessage<FileTransmissionId>(GetArena());
          download_files[entity_id].file_transmission_id->CopyFrom(entity_response->download_id());
        }
      }
    }

    for (const auto &item : download_files) {
      auto file_descriptor(item.second);
      {
        TRANSACTION_SYNCRONIZED_BLOCK
        if (this->status.GetCode() == StatusCode::EXECUTING) {
          LINKAHEAD_LOG_INFO(logger_name) << "Downloading " << file_descriptor.local_path;

          handler_ = std::make_unique<DownloadRequestHandler>(&handler_, file_service.get(),
                                                              &completion_queue, file_descriptor);
        }
      }
      this->status = ProcessCalls();
      if (this->status.GetCode() != StatusCode::EXECUTING) {
        // this indicates an error during the download
        return;
      }
    }
  }
  if (this->status.GetCode() == StatusCode::EXECUTING) {
    ProcessTerminated();
  }
}

auto Transaction::ExecuteAsynchronously() noexcept -> StatusCode {
  TRANSACTION_SYNCRONIZED_BLOCK
  if (this->status.GetCode() != StatusCode::READY && this->status.GetCode() != StatusCode::GO_ON) {
    return StatusCode::TRANSACTION_STATUS_ERROR;
  }
  switch (this->transaction_type) {
  case MIXED_READ_AND_WRITE:
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(
      logger_name, StatusCode::UNSUPPORTED_FEATURE,
      "MIXED_WRITE UNSUPPORTED: The current implementation does not support "
      "mixed read and write transactions (containing retrievals, insertions, "
      "deletions, and updates in one transaction).")
  default:
    break;
  }
  this->status = TransactionStatus::EXECUTING();

  this->transaction_future = std::async(std::launch::async, [this]() { DoExecuteTransaction(); });

  return StatusCode::EXECUTING;
}

auto ProcessSelectResponse(ProtoSelectQueryResult *select_result) -> std::unique_ptr<ResultTable> {
  return ResultTableImpl::create(select_result);
}

auto Transaction::ProcessRetrieveResponse(RetrieveResponse *retrieve_response,
                                          std::vector<std::unique_ptr<Entity>> *entities,
                                          bool *set_error) const noexcept
  -> std::unique_ptr<Entity> {
  std::unique_ptr<Entity> result;
  switch (retrieve_response->retrieve_response_case()) {
  case RetrieveResponseCase::kEntityResponse: {
    auto *retrieve_entity_response = retrieve_response->release_entity_response();
    result = std::make_unique<Entity>(retrieve_entity_response);
  } break;
  case RetrieveResponseCase::kSelectResult: {
    this->result_table = ProcessSelectResponse(retrieve_response->mutable_select_result());
  } break;
  case RetrieveResponseCase::kCountResult: {
    this->query_count = retrieve_response->count_result();
  } break;
  case RetrieveResponseCase::kFindResult: {
    std::unique_ptr<Entity> find_result;
    for (auto &entity_response : *retrieve_response->mutable_find_result()->mutable_result_set()) {
      find_result = std::make_unique<Entity>(&entity_response);
      if (find_result->HasErrors()) {
        *set_error = true;
      }
      entities->push_back(std::move(find_result));
    }
  } break;
  default:
    LINKAHEAD_LOG_FATAL(logger_name) << "Received invalid QueryResponseCase.";
    break;
  }
  return result;
}

auto Transaction::ProcessTerminated() const noexcept -> void {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "Transaction::ProcessTerminated()")
  bool set_error = false;
  auto *responses = this->response->mutable_responses();
  std::vector<std::unique_ptr<Entity>> entities;

  for (auto &sub_response : *responses) {
    std::unique_ptr<Entity> result;
    switch (sub_response.transaction_response_case()) {
    case TransactionResponseCase::kRetrieveResponse: {
      auto *retrieve_response = sub_response.mutable_retrieve_response();
      result = ProcessRetrieveResponse(retrieve_response, &entities, &set_error);
      break; // break TransactionResponseCase::kRetrieveResponse
    }
    case TransactionResponseCase::kInsertResponse: {
      auto *inserted_id_response = sub_response.mutable_insert_response()->mutable_id_response();
      result = std::make_unique<Entity>(inserted_id_response);
      break;
    }
    case TransactionResponseCase::kDeleteResponse: {
      auto *deleted_id_response = sub_response.mutable_delete_response()->mutable_id_response();
      result = std::make_unique<Entity>(deleted_id_response);
      break;
    }
    case TransactionResponseCase::kUpdateResponse: {
      auto *updated_id_response = sub_response.mutable_update_response()->mutable_id_response();
      result = std::make_unique<Entity>(updated_id_response);
      break;
    }
    default:
      LINKAHEAD_LOG_FATAL(logger_name) << "Received invalid TransactionResponseCase.";
      break;
    } // default to sub_response.transaction_response_case()
    if (result != nullptr) {
      if (result->HasErrors()) {
        set_error = true;
      }
      entities.push_back(std::move(result));
    }
  }

  // copy local path of downloaded files into the entities file descriptor
  for (auto &entity : entities) {
    auto id = entity->GetId();
    if (!id.empty() && download_files.count(id) == 1) {
      const auto &local_path = download_files.at(id).local_path;
      entity->SetLocalPath(local_path);
    }
  }
  this->result_set = std::make_unique<MultiResultSet>(std::move(entities));

  if (set_error) {
    this->status = TransactionStatus::TRANSACTION_ERROR("The request terminated with errors.");
  } else {
    this->status = TransactionStatus::SUCCESS();
  }
}

auto Transaction::WaitForIt() const noexcept -> TransactionStatus {
  if (this->status.GetCode() != StatusCode::EXECUTING) {
    return this->status;
  }

  this->transaction_future.wait();

  return this->status;
}

auto Transaction::ProcessCalls() -> TransactionStatus {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "Transaction::ProcessCalls()") {
    TRANSACTION_SYNCRONIZED_BLOCK
    if (this->status.GetCode() != StatusCode::EXECUTING) {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "Transaction::ProcessCalls() was called, TransactionStatus was: "
        << std::to_string(this->status.GetCode()) << " - " << this->status.GetDescription();
      return status;
    }

    handler_->Start();
  }

  gpr_timespec deadline;
  deadline.tv_sec = 1;
  deadline.tv_nsec = 0;
  deadline.clock_type = gpr_clock_type::GPR_TIMESPAN;

  TransactionStatus result = TransactionStatus::EXECUTING();

  void *tag = nullptr;
  bool ok = false;
  while (true) {
    switch (completion_queue.AsyncNext(&tag, &ok, deadline)) {
    case NextStatus::GOT_EVENT: {
      if (tag != nullptr) {
        auto res = handler_->OnNext(ok);
        if (!res) {
          // The handler has finished it's work
          result = handler_->GetStatus();
          return result;
        }
      } else {
        std::string description("Invalid tag delivered by notification queue.");
        LINKAHEAD_LOG_ERROR(logger_name) << description;
        return TransactionStatus::RPC_ERROR(description);
      }
    } break;
    case NextStatus::SHUTDOWN: {
      LINKAHEAD_LOG_ERROR(logger_name) << "Notification queue has been shut down unexpectedly.";
      result = handler_->GetStatus();
      return result;
    } break;
    case NextStatus::TIMEOUT: {
      LINKAHEAD_LOG_DEBUG(logger_name) << "Timeout, waiting...";
    } break;
    default:
      LINKAHEAD_LOG_FATAL(logger_name) << "Got an invalid NextStatus from CompletionQueue.";
      result = handler_->GetStatus();
      return result;
    }
  }

  result = handler_->GetStatus();
  return result;
}

Transaction::~Transaction() {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "Transaction::~Transaction()")
  this->Cancel();
}

void Transaction::Cancel() {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "Transaction::Cancel()")

  if (this->status.GetCode() == StatusCode::CANCELLED) {
    return;
  }

  {

    TRANSACTION_SYNCRONIZED_BLOCK

    if (handler_ != nullptr) {
      handler_->Cancel();
    }

    this->status = TransactionStatus::CANCELLED();

    completion_queue.Shutdown();

    // drain the queue
    void *ignoredTag = nullptr;
    bool ok = false;
    while (completion_queue.Next(&ignoredTag, &ok)) {
      ;
    }
  }

  if (transaction_future.valid()) {
    transaction_future.wait();
  }
}

} // namespace linkahead::transaction
