/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#include "linkahead/unary_rpc_handler.h"
#include "linkahead/logging.h"     // for LINKAHEAD_LOG_TRACE
#include "linkahead/status_code.h" // for GENERIC_RPC_E...
#include <exception>               // IWYU pragma: keep
#include <string>                  // for string, opera...
// IWYU pragma: no_include <bits/exception.h>

namespace linkahead::transaction {

bool UnaryRpcHandler::OnNext(bool ok) {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "UnaryRpcHandler::OnNext(bool)")
  try {
    if (ok) {
      if (state_ == CallState::NewCall) {
        this->handleNewCallState();
      } else if (state_ == CallState::CallComplete) {
        this->handleCallCompleteState();
        return false;
      }
    } else {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "UnaryRpcHandler::OnNext(false)!. This should not happen.";
      // TODO(tf) Handle this error:
      // in CallComplete state: "Client-side Finish: ok should always be true"
      // in ReceivingFile state: "ok indicates that the RPC is going to go to
      // the wire. If it is false, it not going to the wire. This would happen
      // if the channel is either permanently broken or transiently broken but
      // with the fail-fast option. (Note that async unary RPCs don't post a CQ
      // tag at this point, nor do client-streaming or bidi-streaming RPCs that
      // have the initial metadata corked option set.)"
      return false;
    }

    return true;
  } catch (std::exception &e) {
    LINKAHEAD_LOG_ERROR(logger_name) << "UnaryRpcHandler caught an exception: " << e.what();
    transaction_status = TransactionStatus::GENERIC_ERROR(e.what());
    state_ = CallState::CallComplete;
  } catch (...) {
    LINKAHEAD_LOG_ERROR(logger_name) << "Transaction error: unknown exception caught";
    transaction_status =
      TransactionStatus::GENERIC_ERROR("UnaryRpcHandler caught an unknown exception");
    state_ = CallState::CallComplete;
  }

  if (state_ != CallState::NewCall) {
    call_context.TryCancel();
  }

  return false;
}

void UnaryRpcHandler::Cancel() {
  state_ = CallState::CallComplete;
  transaction_status = TransactionStatus::CANCELLED();
  call_context.TryCancel();
}

void UnaryRpcHandler::handleCallCompleteState() {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "UnaryRpcHandler::handleCallCompleteState()")

  switch (status_.error_code()) {
  case grpc::OK:
    LINKAHEAD_LOG_TRACE(logger_name) << "UnaryRpcHandler finished successfully.";
    break;
  default:
    auto code(static_cast<StatusCode>(status_.error_code()));
    std::string description(get_status_description(code) +
                            " Original message: " + status_.error_message());
    transaction_status = TransactionStatus(code, description);
    LINKAHEAD_LOG_ERROR(logger_name)
      << "UnaryRpcHandler finished with an error (Code " << code << "): " << description;
    break;
  }
}

} // namespace linkahead::transaction
