/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/configuration.h"
#include <grpcpp/security/credentials.h> // for SslCredentialsOptions, SslC...
#include <boost/json/impl/object.hpp>    // for object::at, object::empty
#include <boost/json/object.hpp>         // for object
#include <boost/json/string.hpp>         // for string
#include <boost/json/value.hpp>          // for value
#include <boost/json/value_ref.hpp>      // for object
#include <cassert>                       // for assert
#include <cstdlib>                       // for getenv
#include <cstring>                       // for strcmp
#include <exception>                     // for exception
#include <iterator>                      // for next
#include <map>                           // for map
#include <ostream>                       // for basic_ios, basic_ostream
#include <stdexcept>                     // for out_of_range
#include <string>                        // for basic_string, operator+
#include <utility>                       // for pair, move
#include "linkahead/authentication.h"    // for Authenticator, PlainPasswor...
#include "linkahead/connection.h"        // for ConnectionManager
#include "linkahead/constants.h"         // for LIBLINKAHEAD_CONFIGURATION_...
#include "linkahead/exceptions.h"        // for ConfigurationError, Exception
#include "linkahead/log_level.h"         // for LINKAHEAD_LOG_LEVEL_OFF
#include "linkahead/logging.h"           // for FileSinkConfiguration, Cons...
#include "linkahead/status_code.h"       // for StatusCode
#include "linkahead/utility.h"           // for JsonValue, get_home_directory

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define WRAPPED_JSON_CONFIGURATION(obj)                                                            \
  (static_cast<value *>((obj)->json_configuration.wrapped.get()))

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GET_CONNECTIONS                                                                            \
  if (!this->json_configuration.wrapped) {                                                         \
    throw ConfigurationError("This LinkAhead client has not been configured.");                    \
  }                                                                                                \
  assert(WRAPPED_JSON_CONFIGURATION(this)->is_object());                                           \
  const auto &configuration = WRAPPED_JSON_CONFIGURATION(this)->as_object();                       \
  if (!configuration.contains("connections")) {                                                    \
    throw ConfigurationError("This LinkAhead client hasn't any configured connections.");          \
  }                                                                                                \
  const auto &connections_value = configuration.at("connections");                                 \
  if (connections_value.is_null()) {                                                               \
    throw ConfigurationError("This LinkAhead client hasn't any configured connections.");          \
  }                                                                                                \
  assert(connections_value.is_object());                                                           \
  const auto &connections = connections_value.as_object();                                         \
  if (connections.empty()) {                                                                       \
    throw ConfigurationError("This LinkAhead client hasn't any configured connections.");          \
  }

namespace linkahead::configuration {

ConfigurationManager ConfigurationManager::mInstance;

using boost::json::object;
using boost::json::value;
using grpc::InsecureChannelCredentials;
using grpc::SslCredentials;
using grpc::SslCredentialsOptions;
using linkahead::authentication::Authenticator;
using linkahead::authentication::PlainPasswordAuthenticator;
using linkahead::connection::ConnectionManager;
using linkahead::exceptions::ConfigurationError;
using linkahead::logging::ConsoleSinkConfiguration;
using linkahead::logging::FileSinkConfiguration;
using linkahead::logging::LoggingConfiguration;
using linkahead::logging::SinkConfiguration;
using linkahead::logging::SyslogSinkConfiguration;
using linkahead::utility::get_home_directory;
using linkahead::utility::load_json_file;
using linkahead::utility::load_string_file;
using std::filesystem::exists;
using std::filesystem::path;

PemFileCertificateProvider::PemFileCertificateProvider(const path &path) {
  this->certificate_provider = load_string_file(path);
}

auto PemFileCertificateProvider::GetCertificatePem() const -> std::string {
  return this->certificate_provider;
}

PemCertificateProvider::PemCertificateProvider(std::string certificate_provider)
  : certificate_provider(std::move(certificate_provider)) {}

auto PemCertificateProvider::GetCertificatePem() const -> std::string {
  return this->certificate_provider;
}

ConnectionConfiguration::ConnectionConfiguration(std::string host, int port)
  : host(std::move(host)), port(port) {}

auto ConnectionConfiguration::GetHost() const -> std::string { return this->host; }

auto ConnectionConfiguration::GetPort() const -> int { return this->port; }

auto operator<<(std::ostream &out, const ConnectionConfiguration &configuration) -> std::ostream & {
  out << configuration.ToString();
  return out;
}

InsecureConnectionConfiguration::InsecureConnectionConfiguration(const std::string &host, int port)
  : ConnectionConfiguration(host, port) {
  this->credentials = InsecureChannelCredentials();
}

auto InsecureConnectionConfiguration::GetChannelCredentials() const
  -> std::shared_ptr<ChannelCredentials> {
  return this->credentials;
}

auto InsecureConnectionConfiguration::ToString() const -> std::string {
  return "InsecureConnectionConfiguration(" + this->GetHost() + "," +
         std::to_string(this->GetPort()) + ")";
}

TlsConnectionConfiguration::TlsConnectionConfiguration(const std::string &host, int port)
  : ConnectionConfiguration(host, port) {
  SslCredentialsOptions options;
  this->credentials = SslCredentials(options);
}

TlsConnectionConfiguration::TlsConnectionConfiguration(
  const std::string &host, int port, const CertificateProvider &certificate_provider)
  : ConnectionConfiguration(host, port) {
  SslCredentialsOptions options;
  options.pem_root_certs = certificate_provider.GetCertificatePem();
  this->credentials = SslCredentials(options);
}

TlsConnectionConfiguration::TlsConnectionConfiguration(const std::string &host, int port,
                                                       const Authenticator &authenticator)
  : ConnectionConfiguration(host, port) {

  SslCredentialsOptions options;
  this->credentials =
    grpc::CompositeChannelCredentials(SslCredentials(options), authenticator.GetCallCredentials());
}

TlsConnectionConfiguration::TlsConnectionConfiguration(
  const std::string &host, int port, const CertificateProvider &certificate_provider,
  const Authenticator &authenticator)
  : ConnectionConfiguration(host, port) {

  SslCredentialsOptions options;
  options.pem_root_certs = certificate_provider.GetCertificatePem();
  this->credentials =
    grpc::CompositeChannelCredentials(SslCredentials(options), authenticator.GetCallCredentials());
}

auto TlsConnectionConfiguration::GetChannelCredentials() const
  -> std::shared_ptr<ChannelCredentials> {
  return this->credentials;
}

auto TlsConnectionConfiguration::ToString() const -> std::string {
  return "TlsConnectionConfiguration(" + this->GetHost() + "," + std::to_string(this->GetPort()) +
         "," + this->certificate_provider + ")";
}

auto CreateCertificateProvider(const object &from) -> std::unique_ptr<CertificateProvider> {
  std::unique_ptr<CertificateProvider> certificate_provider;
  if (from.contains("server_certificate_path")) {
    const value &path_str = from.at("server_certificate_path");
    assert(path_str.is_string() == true);
    const path certificate_file = path(path_str.as_string().c_str());
    if (!exists(certificate_file)) {
      throw ConfigurationError("File does not exist (server_certificate_path): " +
                               certificate_file.string());
    }
    certificate_provider =
      std::make_unique<PemFileCertificateProvider>(path(path_str.as_string().c_str()));
  }
  return certificate_provider;
}

auto CreateAuthenticator(const object &from) -> std::unique_ptr<Authenticator> {
  std::unique_ptr<Authenticator> authenticator;
  if (from.contains("authentication")) {
    assert(from.at("authentication").is_object());
    auto authentication = from.at("authentication").as_object();
    auto type = std::string("plain");
    if (authentication.contains("type")) {
      assert(authentication.at("type").is_string());
      type = std::string(authentication.at("type").as_string().c_str());
    }
    if (type == "plain") {
      assert(authentication.contains("username"));
      auto username = authentication.at("username");
      assert(username.is_string());

      assert(authentication.contains("password"));
      auto password = authentication.at("password");
      assert(password.is_string());

      authenticator = std::make_unique<PlainPasswordAuthenticator>(
        std::string(username.as_string().c_str()), std::string(password.as_string().c_str()));
    } else {
      throw ConfigurationError("Unknow authentication type: '" + type + "'.");
    }
  }
  return authenticator;
}

auto CreateConnectionConfiguration(const bool tls, const std::string &host, const int port,
                                   const CertificateProvider *certificate_provider,
                                   const Authenticator *authenticator)
  -> std::unique_ptr<ConnectionConfiguration> {
  if (tls) {
    if (certificate_provider != nullptr && authenticator != nullptr) {
      // authenticated and special certificate
      return std::make_unique<TlsConnectionConfiguration>(host, port, *certificate_provider,
                                                          *authenticator);
    } else if (certificate_provider != nullptr) {
      // unauthenticated, special certificate
      return std::make_unique<TlsConnectionConfiguration>(host, port, *certificate_provider);
    } else if (authenticator != nullptr) {
      // authenticated, no special certificate
      return std::make_unique<TlsConnectionConfiguration>(host, port, *authenticator);
    }
    // unauthenticated, no special certificate
    return std::make_unique<TlsConnectionConfiguration>(host, port);
  } else {
    return std::make_unique<InsecureConnectionConfiguration>(host, port);
  }
}

auto IsTls(const object &from) -> bool {
  bool tls = true;
  if (from.contains("tls")) {
    auto tls_switch = from.at("tls");
    assert(tls_switch.is_bool());
    tls = tls_switch.as_bool();
  }
  return tls;
}

auto CreateConnectionConfiguration(const object &from) -> std::unique_ptr<ConnectionConfiguration> {
  assert(from.contains("host"));
  const auto &host = from.at("host");
  assert(host.is_string());

  assert(from.contains("port"));
  const auto &port = from.at("port");
  assert(port.is_int64());

  auto tls = IsTls(from);

  auto certificate_provider = CreateCertificateProvider(from);

  auto authenticator = CreateAuthenticator(from);

  return CreateConnectionConfiguration(tls, std::string(host.as_string().c_str()),
                                       static_cast<int>(port.as_int64()),
                                       certificate_provider.get(), authenticator.get());
}

auto CreateConsoleSinkConfiguration(const object & /*from*/, const std::string &name, int level)
  -> std::shared_ptr<linkahead::logging::SinkConfiguration> {
  auto result = std::make_shared<ConsoleSinkConfiguration>(name, level);
  return result;
}

auto CreateSyslogSinkConfiguration(const object & /*from*/, const std::string &name, int level)
  -> std::shared_ptr<linkahead::logging::SinkConfiguration> {
  auto result = std::make_shared<SyslogSinkConfiguration>(name, level);
  return result;
}

auto CreateFileSinkConfiguration(const object &from, const std::string &name, int level)
  -> std::shared_ptr<linkahead::logging::SinkConfiguration> {
  auto result = std::make_shared<FileSinkConfiguration>(name, level);
  if (from.contains("directory")) {
    result->SetDirectory(from.at("directory").as_string().c_str());
  }
  return result;
}

auto ConvertLogLevel(const std::string &string_level) -> int {
  static std::map<std::string, int> log_level_names = {
    {"", LINKAHEAD_DEFAULT_LOG_LEVEL},    {"off", LINKAHEAD_LOG_LEVEL_OFF},
    {"fatal", LINKAHEAD_LOG_LEVEL_FATAL}, {"error", LINKAHEAD_LOG_LEVEL_ERROR},
    {"warn", LINKAHEAD_LOG_LEVEL_WARN},   {"info", LINKAHEAD_LOG_LEVEL_INFO},
    {"debug", LINKAHEAD_LOG_LEVEL_DEBUG}, {"trace", LINKAHEAD_LOG_LEVEL_TRACE},
    {"all", LINKAHEAD_LOG_LEVEL_ALL}};
  try {
    return log_level_names.at(string_level);
  } catch (const std::out_of_range &exc) {
    throw ConfigurationError("Unknown log level: " + string_level);
  }
}

auto CreateSinkConfiguration(const object &from, const std::string &name, int default_level)
  -> std::shared_ptr<linkahead::logging::SinkConfiguration> {
  assert(from.contains("destination"));
  const auto &destination = std::string(from.at("destination").as_string().c_str());

  int level =
    from.contains("level") ? ConvertLogLevel(from.at("level").as_string().c_str()) : default_level;

  if (destination == "file") {
    return CreateFileSinkConfiguration(from, name, level);
  } else if (destination == "console") {
    return CreateConsoleSinkConfiguration(from, name, level);
  } else if (destination == "syslog") {
    return CreateSyslogSinkConfiguration(from, name, level);
  } else {
    throw ConfigurationError("Unknown sink destination: " + destination);
  }
}

auto CreateLoggingConfiguration(const object &from) -> LoggingConfiguration {
  auto default_level_str =
    from.contains("level") ? std::string(from.at("level").as_string().c_str()) : "";
  int default_level = ConvertLogLevel(default_level_str);

  auto result = LoggingConfiguration(default_level);
  if (default_level == LINKAHEAD_LOG_LEVEL_OFF) {
    return result;
  }

  if (from.contains("sinks")) {
    const auto &sinks = from.at("sinks").as_object();
    if (!sinks.empty()) {
      const auto *elem = sinks.begin();

      while (elem != sinks.end()) {
        result.AddSink(CreateSinkConfiguration(elem->value().as_object(), std::string(elem->key()),
                                               default_level));
        elem = std::next(elem);
      }
    }
  }

  return result;
}

auto ConfigurationManager::mReset() noexcept -> int {
  try {
    mClear();
    InitializeDefaults();
    return StatusCode::SUCCESS;
  } catch (const linkahead::exceptions::Exception &exc) {
    return exc.GetCode();
  } catch (const std::exception &exc) {
    LINKAHEAD_LOG_ERROR(logger_name)
      << "Unknown error during the reset of the ConfigurationManager: " << exc.what();
    return StatusCode::CONFIGURATION_ERROR;
  }
}

auto ConfigurationManager::mClear() noexcept -> int {
  try {
    json_configuration.Reset();
    ConnectionManager::Reset();
    return StatusCode::SUCCESS;
  } catch (const linkahead::exceptions::Exception &exc) {
    return exc.GetCode();
  } catch (const std::exception &exc) {
    LINKAHEAD_LOG_ERROR(logger_name)
      << "Unknown error during the reset of the ConfigurationManager: " << exc.what();
    return StatusCode::CONFIGURATION_ERROR;
  }
}

auto ConfigurationManager::mLoadSingleJSONConfiguration(const path &json_file) -> void {
  if (json_configuration.wrapped) {
    throw ConfigurationError("This LinkAhead client has already been configured.");
  }
  if (!exists(json_file)) {
    throw ConfigurationError("Configuration file does not exist.");
  }

  json_configuration = load_json_file(json_file);
}

auto ConfigurationManager::mGetConnectionConfiguration(const std::string &name) const
  -> std::unique_ptr<ConnectionConfiguration> {
  GET_CONNECTIONS
  if (connections.contains(name)) {
    const auto &result_connection = connections.at(name);
    assert(result_connection.is_object());
    return CreateConnectionConfiguration(result_connection.as_object());
  }
  throw ConfigurationError("The connection '" + name + "' has not been defined.");
}

auto ConfigurationManager::mGetDefaultConnectionName() const -> std::string {
  GET_CONNECTIONS
  if (connections.contains("default")) {
    auto default_connection = connections.at("default");
    if (default_connection.is_object()) {
      // the name is actually "default"
      return {"default"};
    } else {
      assert(default_connection.is_string());
      auto default_connection_name = default_connection.as_string();
      // return the string value of connections.default
      return {default_connection_name.c_str()};
    }
  }
  if (connections.size() == 1) {
    // return the key of the first and only sub-element of connections.
    return std::string(connections.begin()->key());
  }
  throw ConfigurationError("Could not determine the default connection.");
}

ConfigurationManager &ConfigurationManager::GetInstance() { return mInstance; }

// TODO(tf) This has apparently a cognitive complexity of 34>25 (threshold).
auto ConfigurationManager::InitializeDefaults() -> int { // NOLINT

  // find the configuration file...
  std::unique_ptr<path> configuration_file_path;
  for (const char *const &configuration_file :
       linkahead::LIBLINKAHEAD_CONFIGURATION_FILES_PRECEDENCE) {
    if (strcmp(configuration_file, "$LINKAHEAD_CLIENT_CONFIGURATION") == 0) {
      // user specified a file via the environment variable
      // TODO(tf) make this thread-secure (concurrency-mt-unsafe)
      const auto *from_env_var = getenv("LINKAHEAD_CLIENT_CONFIGURATION"); // NOLINT
      if (from_env_var != nullptr) {
        configuration_file_path = std::make_unique<path>(from_env_var);
        if (exists(*configuration_file_path)) {
          break;
        } else {
          configuration_file_path = nullptr;
          // TODO(tf) log warning: "Configuration file under
          // $LINKAHEAD_CLIENT_CONFIGURATION does not exist.
        }
      }
    } else {
      // check standard locations
      configuration_file_path = std::make_unique<path>();
      const path raw(configuration_file);
      // resolve home directory
      for (const auto &segment : raw) {
        if (segment.string() == "$HOME") {
          path expanded_home(get_home_directory());
          *configuration_file_path /= expanded_home;
        } else {
          *configuration_file_path /= segment;
        }
      }
      if (exists(*configuration_file_path)) {
        break;
      } else {
        configuration_file_path = nullptr;
      }
    }
  }

  // ... and use the configuration file
  if (configuration_file_path != nullptr) {
    mLoadSingleJSONConfiguration(*configuration_file_path);
  }

  // Logging in the configuration leads to additional content.
  if (this->json_configuration.wrapped && WRAPPED_JSON_CONFIGURATION(this)->is_object() &&
      WRAPPED_JSON_CONFIGURATION(this)->as_object().contains("logging")) {
    LoggingConfiguration logging_configuration =
      CreateLoggingConfiguration(WRAPPED_JSON_CONFIGURATION(this)->at("logging").as_object());
    logging::initialize_logging(logging_configuration);
  } else {
    logging::initialize_logging_defaults();
    LINKAHEAD_LOG_INFO(logger_name) << "No logging configuration has been found. "
                                       "We are using the default configuration";
  }

  if (configuration_file_path != nullptr && this->json_configuration.wrapped &&
      WRAPPED_JSON_CONFIGURATION(this)->is_object()) {
    LINKAHEAD_LOG_INFO(logger_name)
      << "Loaded configuration from " << *(configuration_file_path) << ".";
  }

  return 0;
}

const int IS_INITIALIZED = ConfigurationManager::Reset();

} // namespace linkahead::configuration
