/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef LINKAHEAD_RESULT_TABLE_IMPL_H
#define LINKAHEAD_RESULT_TABLE_IMPL_H

#include "linkahead/transaction.h"
#include "caosdb/entity/v1/main.pb.h"                                 // for EntityTransac...
#include "caosdb/entity/v1/main.pb.h"                                 // for TransactionRe...
#include "linkahead/file_transmission/download_request_handler.h"     // Download...
#include "linkahead/file_transmission/file_reader.h"                  // for path
#include "linkahead/file_transmission/register_file_upload_handler.h" // for RegisterFileUploadHandler
#include "linkahead/file_transmission/upload_request_handler.h"       // Upload...
#include "linkahead/logging.h"                                        // for LINKAHEAD_LOG_FATAL
#include "linkahead/protobuf_helper.h"                                // for ProtoMessageWrapper
#include "linkahead/status_code.h"                                    // for StatusCode
#include "linkahead/transaction_handler.h" // for EntityTransactionHandler
#include <algorithm>                       // for max
#include <filesystem>                      // for operator<<, path
#include <future>                          // for async, future
#include <google/protobuf/arena.h>         // for Arena
#include <grpc/grpc.h>                     // for gpr_timespec
#include <map>                             // for map, operator!=
#include <memory>                          // for unique_ptr
#include <random>                          // for mt19937, rand...
#include <system_error>                    // for std::system_error
#include <utility>                         // for move, pair

namespace linkahead::transaction {
using linkahead::entity::Value;
using ProtoSelectQueryResult = caosdb::entity::v1::SelectQueryResult;
using ProtoSelectQueryHeader = caosdb::entity::v1::SelectQueryHeader;
using ProtoSelectQueryColumn = caosdb::entity::v1::SelectQueryColumn;
using ProtoSelectQueryRow = caosdb::entity::v1::SelectQueryRow;
using linkahead::utility::ScalarProtoMessageWrapper;

class ResultTableRowImpl {
  explicit ResultTableRowImpl(ProtoSelectQueryResult *table, int row);
  [[nodiscard]] auto GetValue(const std::string &column) const noexcept -> Value;
  [[nodiscard]] auto GetColumnIndex(const std::string &column) const noexcept -> int;
  friend class ResultTableRow;
  friend class ResultTableImpl;
  ProtoSelectQueryHeader &header;
  ProtoSelectQueryRow &row;
};

class ResultTableColumnImpl : public ScalarProtoMessageWrapper<ProtoSelectQueryColumn> {
  explicit ResultTableColumnImpl(ProtoSelectQueryColumn *column);
  friend class ResultTableColumn;
  friend class ResultTableImpl;
};

class ResultTableImpl : public ScalarProtoMessageWrapper<ProtoSelectQueryResult> {
  static auto create(ProtoSelectQueryResult *select_result) -> std::unique_ptr<ResultTable>;
  ResultTableImpl();
  explicit ResultTableImpl(ProtoSelectQueryResult *result_table);
  std::vector<ResultTableColumn> columns;
  std::vector<ResultTableRow> rows;

  friend class ResultTable;
  friend class ResultTable::HeaderIterator;
  friend class ResultTableColumn;
  friend auto ProcessSelectResponse(ProtoSelectQueryResult *select_result)
    -> std::unique_ptr<ResultTable>;
};

} // namespace linkahead::transaction
#endif
