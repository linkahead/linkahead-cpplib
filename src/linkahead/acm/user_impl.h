/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/acm/user.h"
#include "caosdb/acm/v1alpha1/main.pb.h" // for ProtoUser
#include "linkahead/protobuf_helper.h"   // for ProtoMessageWrapper
#include <memory>                        // for unique_ptr
#include <utility>                       // for move

namespace linkahead::acm {
using linkahead::utility::ScalarProtoMessageWrapper;
using ProtoUser = caosdb::acm::v1alpha1::User;

/**
 * UserImpl class is designed to hide the implementation which makes direct use
 * of the protobuf objects underneath from the clients of the caosdb library.
 */
class UserImpl : public ScalarProtoMessageWrapper<ProtoUser> {
public:
  UserImpl();
  /**
   * Constructor. Instanciate a user with the given name.
   */
  explicit UserImpl(std::string name);
  /**
   * Constructor. Instanciate a user with the given realm and name.
   */
  UserImpl(std::string realm, std::string name);
  /**
   * Constructor. Instanciate a user from the server's responces.
   */
  UserImpl(ProtoUser *user);

  friend class User;
  friend class linkahead::connection::Connection;

private:
  std::string password;
};

} // namespace linkahead::acm
