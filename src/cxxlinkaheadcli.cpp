/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

// A simple linkahead client
#include "linkahead/connection.h"         // for Connection, ConnectionManager
#include "linkahead/constants.h"          // for LIBLINKAHEAD_VERSION_MINOR, LIBLINKAHEAD_V...
#include "linkahead/entity.h"             // for Entity
#include "linkahead/exceptions.h"         // for ConfigurationError
#include "linkahead/info.h"               // for VersionInfo
#include "linkahead/logging.h"            // for LINKAHEAD_LOG_TRACE
#include "linkahead/transaction.h"        // for Transaction, ResultSet
#include "linkahead/transaction_status.h" // for TransactionSt...
#include <boost/log/core/record.hpp>      // for record
#include <boost/log/detail/attachable_sstream_buf.hpp> // for basic_ostring...
#include <boost/log/sources/record_ostream.hpp>        // for operator<<
#include <boost/preprocessor/seq/limits/enum_256.hpp>  // for BOOST_PP_SEQ_...
#include <boost/preprocessor/seq/limits/size_256.hpp>  // for BOOST_PP_SEQ_...
#include <exception>                                   // for exception
#include <iostream> // for operator<<, basic_ostream, basic_ost...
#include <memory>   // for unique_ptr, allocator, __shared_ptr_...
#include <string>   // for operator<<, char_traits

const auto logger_name = "liblinkahead";

auto main() -> int {

  std::cout << "LinkAhead C++ client (liblinkahead " << linkahead::LIBLINKAHEAD_VERSION_MAJOR << "."
            << linkahead::LIBLINKAHEAD_VERSION_MINOR << "." << linkahead::LIBLINKAHEAD_VERSION_PATCH
            << ")\n";

  try {
    const auto &connection = linkahead::connection::ConnectionManager::GetDefaultConnection();

    auto status = connection->RetrieveVersionInfoNoExceptions();
    if (status.GetCode() == linkahead::CONNECTION_ERROR) {
      const auto connection_config =
        linkahead::configuration::ConfigurationManager::GetDefaultConnectionConfiguration();
      std::cout << "ConnectionError: Cannot connect to " << connection_config->GetHost() << ":"
                << connection_config->GetPort() << '\n';

      return 1;
    }
    // get version info of the server
    const auto &v_info = connection->GetVersionInfo();
    std::cout << "Server Version: " << v_info->GetMajor() << "." << v_info->GetMinor() << "."
              << v_info->GetPatch() << "-" << v_info->GetPreRelease() << "-" << v_info->GetBuild()
              << '\n';

    // retrieve an entity
    auto transaction(connection->CreateTransaction());
    transaction->RetrieveById("21");
    transaction->ExecuteAsynchronously();
    auto t_stat = transaction->WaitForIt();
    LINKAHEAD_LOG_INFO(logger_name)
      << "status: " << t_stat.GetCode() << " // " << t_stat.GetDescription();
    const auto &result_set = transaction->GetResultSet();
    for (const auto &entity : result_set) {
      std::cout << entity.ToString() << '\n';
    }

    // execute a query
    std::string query("FIND Property \"Prop *\"");
    std::cout << "Trying to execute a query:\n" << query << '\n';
    auto q_transaction(connection->CreateTransaction());
    q_transaction->Query(query);
    q_transaction->ExecuteAsynchronously();
    t_stat = q_transaction->WaitForIt();
    LINKAHEAD_LOG_INFO(logger_name)
      << "status: " << t_stat.GetCode() << " // " << t_stat.GetDescription();
    const auto &result_set_2 = q_transaction->GetResultSet();
    for (const auto &entity : result_set_2) {
      std::cout << entity.ToString() << '\n';
    }

    return 0;
  } catch (const linkahead::exceptions::ConfigurationError &exc) {
    std::cout << "ConfigurationError: " << exc.what() << '\n';
    return exc.GetCode();
  } catch (const std::exception &exc) {
    std::cout << "Exception: " << exc.what() << '\n';
    return 1;
  } catch (...) {
    std::cout << "Some other exception." << '\n';
    return 2;
  }
}
