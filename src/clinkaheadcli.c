#include "linkahead/constants.h" // for LIBLINKAHEAD_VERSION_MAJOR, LIBLINKAHEAD_VER...
#include "clinkahead.h"          // for linkahead_info_version_info, linkahead_conne...
#include <stdio.h>               // for printf

int main(void) {
  int status = 0; // last function return value
  printf("LinkAhead C client (liblinkahead %d.%d.%d)\n\n", LIBLINKAHEAD_VERSION_MAJOR,
         LIBLINKAHEAD_VERSION_MINOR, LIBLINKAHEAD_VERSION_PATCH);

  linkahead_connection_connection connection;
  status = linkahead_connection_connection_manager_get_default_connection(&connection);
  if (status != 0) {
    printf("An error occured: ERROR %d - %s\n", status, linkahead_get_status_description(status));
    return status;
  }

  linkahead_info_version_info version_info;
  status = linkahead_connection_get_version_info(&version_info, &connection);
  if (status != 0) {
    printf("An error occured: ERROR %d - %s\n", status, linkahead_get_status_description(status));
    return status;
  }

  printf("Server version: %d.%d.%d-%s-%s\n", version_info.major, version_info.minor,
         version_info.patch, version_info.pre_release, version_info.build);

  return 0;
}
